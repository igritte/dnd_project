<?php
$stats = ['strength', 'constitution', 'dexterity', 'intelligence', 'wisdom', 'charisma'];

function connect_to_db($host="localhost", $user="root", $password="", $database){
	$link = mysqli_connect($host, $user, $password, $database);
	return $link;
}

function stat_select() {
	
}

function upload_file($file) {
	print_r($_FILES);
	/*$target_path = "src/uploads/";
	
	$target_path = $target_path . basename( $_FILES[$file]['name']);
	
	if (move_uploaded_file($_FILES[$file]['tmp_name'], $target_path)) {
		echo "The file ".  basename( $_FILES[$file]['name']).
		" has been uploaded";
	} else {
		echo "There was an error uploading the file, please try again!";
	}*/
}
function get_skills($class) {
	$skills = array();
	$link = connect_to_db("localhost", "root", "", "dnd_project");
	$query = "SELECT c.class_name, s.skill_name, s.description FROM skills s JOIN class_to_skill t ON t.skill_id=s.id JOIN classes c ON c.id=t.class_id WHERE c.playable=1 and c.class_name='$class'";
	
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	if(mysqli_num_rows($result) > 0) {
		while ($row = mysqli_fetch_assoc($result)) {
			$skills[$row['class_name']][$row['skill_name']] = $row['description'];
		}
	}
	mysqli_free_result($result);
	mysqli_close($link);
	return $skills;
}
if (isset($_POST['skills'])) {
	$s = json_encode(get_skills($_POST['skills']));
	echo $s;
}

function get_races()
{
	$races = array();
	$link = connect_to_db("localhost", "root", "", "dnd_project");
	$query = "SELECT race_name, description FROM races WHERE playable=1";

	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	if(mysqli_num_rows($result) > 0) {
		while ($row = mysqli_fetch_assoc($result)) {
			$races[$row['race_name']] = $row['description'];
		}
	}
	mysqli_free_result($result);
	mysqli_close($link);
	return $races;
}

function get_race_selection($gender="male")
{
	$races = get_races();
	foreach ($races as $race => $description) {
		echo "<div class=\"race_picker\"><p class=\"character_title\">" . ucfirst($race)
		.  "</p><div class=\"race_img\" id=\"$race\"></div></div>";
	}
}

function get_classes()
{
	$classes = array();
	$link = connect_to_db("localhost", "root", "", "dnd_project");
	$query = "SELECT class_name, description FROM classes";

	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	if(mysqli_num_rows($result) > 0) {
		while ($row = mysqli_fetch_assoc($result)) {
			$classes[$row['class_name']] = $row['description'];
		}
	}
	mysqli_free_result($result);
	mysqli_close($link);
	return $classes;
}

function get_class_selection($race="all")
{
	$classes = get_classes();
	if ($race == "all") {
		foreach ($classes as $class => $description) {
			echo "<div class=\"class_picker\" id=$class><p class=\"character_title\">" . ucfirst($class)
			.  "</p>$description</div>";
		}
	}
}

function get_stats()
{
	$stats = array();
	$link = connect_to_db("localhost", "root", "", "dnd_project");
	$query = "SELECT stat_name FROM stats";

	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	if(mysqli_num_rows($result) > 0) {
		while ($row = mysqli_fetch_assoc($result)) {
			$stats[$row['stat_name']] = 10;
		}
	}
	mysqli_free_result($result);
	mysqli_close($link);
	return $stats;
}

if(isset($_POST['action']) && !empty($_POST['action'])) {
	$action = $_POST['action'];
	get_race_selection($action);
}

if(isset($_POST['upload']) && !empty($_POST['upload'])) {
	$file = $_POST['upload'];
	upload_file($file);
}

function is_logged_in()
{
	if (isset($_SESSION['username']))
		return true;
	else
		return false;	
}
function selected_char()
{
	if (isset($_SESSION['character']))
		return true;
	else
		return false;	
}