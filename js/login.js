/*
 * 
 */
var initHomeApp = function() {
	
	$('#reg_username').focusout( function() {
		$('#email_error').removeAttr('class').html('');
		$('#user_error').removeAttr('class').html('');
		if ($('#reg_username').val() != '') {
			request = $.ajax({
			type: "POST",
			url: 'register.php',
			data: {'validate_user': $(this).val()},
			success: function(response){
				if (response != '')
					$('#user_error').addClass('error').html(response);
	        }
			});
		}
	});
	
	$('#email').focusout( function() {
		if ($('#email').val() != '') {
			request = $.ajax({
			type: "POST",
			url: 'register.php',
			data: {'validate_email': $(this).val()},
			success: function(response){
				if (response != '')
					$('#email_error').addClass('error').html(response);
	        	}
			});
		}
	});
	
	$('#reg_pass').focusout( function() {
		if ($('#reg_pass').val() != '') {
			request = $.ajax({
			type: "POST",
			url: 'register.php',
			data: {'validate_pass': $(this).val()},
			success: function(response){
				if (response != '')
					$('#pass_error').addClass('error').html(response);
	        }
			});
		} else {
			$('#pass_error').removeClass('error');
		}
	});
	//use config's url path
	$('#reg_form').on("submit", function(event){
		event.preventDefault();
		   $.ajax({
		       url: $('#reg_form').attr("action"),
		       data: $('#reg_form').serialize(),
		       type: "POST",
		       dataType: "json",
		       success: function(response) {
					if (response.status === 'fail') {
						$('#user_error').addClass('error').html(response.msg);
					} else {
						window.location.replace("http://localhost/kurs/DnD/character");
					}
				}
		  });
		  return false;
		});
	
	$('#log_me_in').on("submit", function(event){
		event.preventDefault();
		   $.ajax({
		       url: $('#log_me_in').attr("action"),
		       data: $('#log_me_in').serialize(),
		       type: "POST",
		       dataType: "json",
		       success: function(response) {
					if (response.status === 'fail') {
						$('#login_error').addClass('error').html(response.msg);
					} else {
						console.log('no idea');
						window.location.replace("http://localhost/kurs/DnD/character");
					}
				}
		  });
		  return false;
		});
	
	$('#reg_pass').on('input', function() {
		if ($(this).val() != '') {
			test_pass($(this).val(), $('#pass_error'));
		} else {
			$('#pass_error').removeClass().html('');
		}
	});
	
	$('#reg_rpass').on('input', function() {
		if ($(this).val() != '' && $(this).val() != $('#reg_pass').val()) {
			$('#rpass_error').addClass('error').html('Passwords do not match');
		} else {
			$('#rpass_error').removeClass().html('');
		}
	});
	
	var MediumPass = /^(?=\S*?[a-zA-Z])(?=\S*?[0-9])\S{5,}$/; //Must contain lower case letters and at least one digit.
	//Must contain at least one upper case letter or one lower case letter and one digit and a special character.
	var StrongPass = /^(?=\S*?[A-Za-z])(?=\S*?[0-9])(?=\S*?[!-_.*])(?=\S*?[^\w\*])\S{5,}$/;
	
	var test_pass = function(pass, error) {
		if(StrongPass.test(pass)) {
			error.removeClass().addClass('strongpass').html('Strong!');
		} else if (MediumPass.test(pass)) {
			error.removeClass().addClass('mediumpass').html('ok');
		} else {
			error.removeClass().addClass('weakpass').html('weak');
		}
	};
};
	initHomeApp();

