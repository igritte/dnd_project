<?php
class LoginModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * login: checks if the user already exists in the database
	 * @param string $username
	 * @param string $password
	 * @return boolean true if success, false otherwise
	 * @todo crypt the password before sending
	 */
	public function login($username, $password)
	{
		$this->db->prepare("SELECT * FROM users WHERE username='$username' and password='$password'");
		$this->db->execute();
		@session_start();
		if ($this->db->rowCount() == 1) {
			$data = $this->db->getResult();
			foreach ($data as $row) {
				$_SESSION["user_id"] = $row['id'];
			}
			$this->getSelectedChar($username);
			return true;
		}
		else
			return false;
	}
	
	/**
	 * register
	 * saves the user in the database
	 * @param string $username
	 * @param string $email
	 * @param string $password
	 * @todo return true/false and check for errors
	 */
	public function register($username, $email, $password)
	{
		$this->db->prepare("INSERT INTO users(username, email, password) VALUES(:username,:email,:password)");
		$this->db->bind(":username", $username);
		$this->db->bind(":email", $email);
		$this->db->bind(":password", $password);
		$this->db->execute();
	}
	
	public function getSelectedChar($username)
	{
		$this->db->prepare("SELECT * FROM characters c JOIN users u ON u.id=c.user_id WHERE username='$username' and c.selected=1");
		$this->db->execute();
		if ($this->db->rowCount() > 0) {
			$data = $this->db->getResult();
			$_SESSION['char'] = $data[0]['name'];
			$_SESSION['char_id'] = $data[0]['id'];
		}
	}
}