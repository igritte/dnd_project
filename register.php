<?php
include_once 'application/utils/Autoloader.php';
//include_once 'application/config.php';
session_start();

function user_exists($username) {
	$query = "SELECT * FROM users WHERE username='$username'";
	$mysqli = new mysqli(Config::getAttribute('host'), Config::getAttribute('username'), Config::getAttribute('password'), Config::getAttribute('database'));
	$stmt = $mysqli->prepare($query);
	$stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	if ($result->num_rows == 0) {
		return false;
	} else {
		return true;
	}
}

function register_user($username, $email, $password) {
	//$link = DB::getInstance()->getConnection();
	$password = sha1($password);
	if (!user_exists($username)) {
		$query = "INSERT INTO users(username, email, password) VALUES('$username', '$email', '$password')";
		$mysqli = new mysqli(Config::getAttribute('host'), Config::getAttribute('username'), Config::getAttribute('password'), Config::getAttribute('database'));
		$stmt = $mysqli->prepare($query);
		$stmt->execute();
		$stmt->close();
		setcookie('username', $username);
		$_SESSION['username']= $username;
	} else {
		echo "username is taken.";
	}
}

function login_user($username, $password, $remember) {
	$password = sha1($password);
	$query = "SELECT * FROM users WHERE username='$username' AND password='$password'";
	$mysqli = new mysqli(Config::getAttribute('host'), Config::getAttribute('username'), Config::getAttribute('password'), Config::getAttribute('database'));
	$stmt = $mysqli->prepare($query);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result->num_rows == 0) {
		echo "invalid username or password";
	} else {
		if ($remember) {
			setcookie('username', $username);
			$_SESSION['username']= $username;
		}
	}
	$stmt->close();
}

if (isset($_POST['username']) && isset($_POST['email']) && isset($_POST['password'])) {
	register_user($_POST['username'], $_POST['email'], $_POST['password']);
}

if (isset($_POST['login_username']) && isset($_POST['login_pass'])  && isset($_POST['remember'])) {
	login_user($_POST['login_username'], $_POST['login_pass'], $_POST['remember']);
}

if (isset($_POST['validate_user']) && !empty($_POST['validate_user'])) {
	$userValidator = new UsernameValidator();
	if (!$userValidator->isValid($_POST['validate_user'])) {
		echo $userValidator->getMessage();
	}
}

if (isset($_POST['validate_email']) && !empty($_POST['validate_email'])) {
	$emailValidator = new EmailValidator();
	if (!$emailValidator->isValid($_POST['validate_email'])) {
		echo $emailValidator->getMessage();
	}
}

if (isset($_POST['validate_pass']) && !empty($_POST['validate_pass'])) {
	$passValidator = new PasswordValidator();
	if (!$passValidator->isValid($_POST['validate_pass'])) {
		echo $passValidator->getMessage();
	}
}