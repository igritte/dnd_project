<?php if (isset ($_SESSION['username'])) {
	header("Location:".URL."character");
}?>
<div id="main">
	<div class="top">
		<img alt="orcs_playing_dnd" src="<?php echo URL;?>styles/1.jpg">
	</div>
	<div class="divider"></div>
	<div class="play_button" onmousedown="$('#login_form').show();$('#outer').css('background-color', 'rgba(255,255,255,0.5)');$('#outer').css('z-index', '11');"><a href="">Play</a></div>
	<div class="page">
		<div class="content">
			<p><span class="start">Imagine</span>  a world of bold warriors, mighty
				wizards, and terrible monsters.
				Imagine a world of ancient ruins, vast caverns, and
				great wild wastes where only the bravest heroes dare
				to tread.</p>
				<img alt="halfling" class="left" src="<?php echo URL;?>styles/halfling.png">
			<p><span class="start">Imagine</span>  a world of swords and magic, a world of
				elves and goblins, a world of giants and dragons.
				This is the world of the DUNGEONS &amp; DRAGONS®
				Roleplaying Game (also referred to as D &amp; D), the
				pinnacle of fantasy roleplaying games. You take
				on the role of a legendary hero—a skilled fighter, a
				courageous cleric, a deadly rogue, or a spell-hurling
				wizard. With some willing friends and a little
				imagination, you strike out on daring missions and
				epic quests, testing yourself against an array of
				daunting challenges and bloodthirsty monsters.</p>
				<img alt="elf" class="right" src="<?php echo URL;?>styles/elf.png">
			<p><span class="start">Get ready </span>— the Player’s Handbook contains
				everything you need to create a heroic character of
				your own! To start you on your first adventure, this
				chapter discusses the following topics.</p>
			<ul>
				<li> A Roleplaying Game: How the D &amp; D game is
						different from any other game you’ve played.</li>
				<li> What’s in a D &amp; D Game: The essential
						ingredients of the DUNGEONS &amp; DRAGONS game.</li>
				<li> How Do You Play?: A look at what happens
						during the game, including a brief example of
						activity at the game table.</li>
				<li> The Core Mechanic: The single fundamental rule
						you need to know for most challenges you face in
						the game.</li>
					</ul>
			<p>The DUNGEONS &amp; DRAGONS game is a roleplaying
				game. In fact, D &amp; D invented the roleplaying game
				and started an industry.</p>
			<p><span class="start">A roleplaying game</span> is a storytelling game that has
				elements of the games of make-believe that many of
				us played as children. However, a roleplaying game
				such as D &amp; amp;D provides form and structure, with
				robust gameplay and endless possibilities.</p>
				<img alt="dwarf" class="left" src="<?php echo URL;?>styles/dwarf.png">
			<p><span class="start">D &amp; D </span>is a fantasy-adventure game. You create
				a character, team up with other characters (your
				friends), explore a world, and battle monsters. While
				the D &amp; D game uses dice and miniatures, the action
				takes place in your imagination. There, you have the
				freedom to create anything you can imagine, with an
				unlimited special effects budget and the technology
				to make anything happen.</p>
			<p><span class="start">What makes</span> the D &amp; D game unique is the
				Dungeon Master. The DM is a person who takes on
				the role of lead storyteller and game referee. The DM
				creates adventures for the characters and narrates
				the action for the players. The DM makes D &amp; D infinitely
				f lexible—he or she can react to any situation,
				any twist or turn suggested by the players, to make a
				D &amp; D adventure vibrant, exciting, and unexpected.
				The adventure is the heart of the D &amp; D game. It’s
				like a fantasy movie or novel, except the characters
				that you and your friends create are the stars of the
				story. The DM sets the scene, but no one knows what’s
				going to happen until the characters do something—
				and then anything can happen! You might explore
				a dark dungeon, a ruined city, a lost temple deep in
				a jungle, or a lava-filled cavern beneath a mysterious
				mountain. You solve puzzles, talk with other
				characters, battle all kinds of fantastic monsters, and
				discover fabulous magic items and treasure.
				D &amp; D is a cooperative game in which you and your
				friends work together to complete each adventure
				and have fun. It’s a storytelling game where the only
				limit is your imagination. It’s a fantasy-adventure
				game, building on the traditions of the greatest
				fantasy stories of all time. In an adventure, you can
				attempt anything you can think of. Want to talk to
				the dragon instead of fighting it? Want to disguise
				yourself as an orc and sneak into the foul lair? Go
				ahead and give it a try. Your actions might work or
				they might fail spectacularly, but either way you’ve
				contributed to the unfolding story of the adventure
				and probably had fun along the way.</p>
		</div>
	</div>
</div>
<div class="bottom">
	<img alt="border" src="<?php echo URL;?>styles/border.gif"></div>
		</div>
	</div>
<script type="text/javascript" src="<?php echo URL;?>js/login.js"></script>
<script>
$( document ).ready(function() {
	console.log( "ready!" );
	console.log($('#log_me_in'));
	var activeurl = window.location;
	$('a[href="'+activeurl+'"]').parent('li').addClass('active');
	console.log(activeurl);
	});</script>