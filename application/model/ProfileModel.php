<?php
class ProfileModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * edit
	 * saves user's new password in the database
	 * @param string $username
	 * @param string $password
	 * @return boolean true on success, false otherwise
	 */
	public function edit($username, $password)
	{
		try {
			$this->db->prepare("UPDATE users SET password=:password WHERE username=:username;");
			$this->db->bind(":username", $username);
			$this->db->bind(":password", $password);
			$this->db->execute();
			return true;
		} catch (Exception $e) {
			//log this exception
			return false;
		}
	}
	
	/**
	 * getCharacters
	 * returns all characters based on user id saved in the session
	 * @param int $user_id
	 * @return multitype:|boolean
	 */
	public function getCharacters($user_id)
	{
		try {
			$this->db->prepare("SELECT cl.class_name, c.*, r.race_name FROM classes cl
					JOIN characters c ON c.class_id=cl.id
					JOIN races r ON r.id=c.race_id WHERE user_id=$user_id");
			$this->db->execute();
			$data = $this->db->getResult();
			return $data;
		} catch (Exception $e) {
			return false;
		}
	}
	
	public function saveChar($char)
	{
		$user_id = $_SESSION['user_id'];
		try {
			$this->db->prepare("UPDATE characters SET selected=0 WHERE user_id=$user_id");
			$this->db->execute();
			$this->db->prepare("UPDATE characters SET selected=1 WHERE id=$char");
			$this->db->execute();
			$this->db->select("characters", "*", "id", $char);
			$this->db->execute();
			$character = $this->db->getResult();
			$race = $character[0]['race_id'];
			$this->db->select("races", "race_name", "id", $race);
			$this->db->execute();
			$race = $this->db->getResult();
			$_SESSION['character'] = $character[0]['name'];
			$_SESSION['gender'] = $character[0]['gender'];
			$_SESSION['char_race'] = $race[0]['race_name'];
		} catch (Exception $e) {
			return false;
		}
	}
	
	public function getCurrentChar($user_id)
	{
		try {
			$this->db->prepare("SELECT id, name, gender FROM characters WHERE user_id=$user_id AND selected=1");
			$this->db->execute();
			$data = $this->db->getResult();
			return $data;
		} catch (Exception $e) {
			return false;
		}
	}
}