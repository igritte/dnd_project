<?php
class Model
{
	public $db;
	
	//to do: move pdo out of the constructor into contructor arguments
	function __construct()
	{
		$options = array(
		    PDO::ATTR_PERSISTENT => true,
		    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
		);
		$pdo = new PDO(Config::getAttribute("dbtype") . ":host=" .
				Config::getAttribute("host") . ";dbname=" .
				Config::getAttribute("database"),
				Config::getAttribute("username"),
				Config::getAttribute("password"), $options);
		$this->db = new Database($pdo);
	}
}