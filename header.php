<?php
// Old code: not being used
session_start();

function is_home_page()
{
	if (basename($_SERVER['PHP_SELF']) == "index.php") {
		return true;
	} else {
		return false;
	}
}

function get_header()
{
	if (is_home_page()) {
		echo "<!DOCTYPE html> <html><head>
	<meta charset=\"UTF-8\">
	<link rel=\"stylesheet\" type=\"text/css\" href=\"styles/styles-front.css\">
	<title>D&amp;D Miniatures</title>
	<script src=\"http://code.jquery.com/jquery-latest.min.js\"
			type=\"text/javascript\"></script>
				<script src=\"js/lib/require.js\"></script>
			</head>";
	} else {
		echo "<!DOCTYPE html><html><head>
	<meta charset=\"UTF-8\">
	<link rel=\"stylesheet\" type=\"text/css\" href=\"styles/styles.css\">
	<title>D&amp;D Miniatures</title>
	<script src=\"http://code.jquery.com/jquery-latest.min.js\"
			type=\"text/javascript\"></script>
				<script src=\"js/lib/require.js\"></script>
			</head>";
	}

}