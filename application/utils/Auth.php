<?php

class Auth
{
	public static function isUserOnline()
	{
		@session_start();
		
		if (!isset($_SESSION['username'])) {
			session_destroy();
			unset($_SESSION);
			header('location: ' . URL . 'login');
		}
	}
	
	public static function selectedChar()
	{
		return isset($_SESSION['character']);
	}
}