<?php include 'helper.php';?>
<style>
#profile .race {
	height: 128px;
	width: 128px;
	position: absolute;
	right: 0;
	top: -25px;
}
</style>
<div class="wrap">
	<div class="header">	
	<div class="play_button"><a href="<?php echo URL."play"?>">Play</a></div>
		<div class="logo"></div>
		<header class="clear">
			<nav id="nav_menu">
				<ul>
					<li><a href="<?php echo URL;?>home" class="home" title="Home">Home</a></li>
					<li><a href="<?php echo URL;?>mapeditor" class="media" title="Media">Media</a></li>
					<li><a href="<?php echo URL;?>dungeon" class="tavern" title="Tavern">Tavern</a></li>
					<li><a href="<?php echo URL;?>character" class="game_info" title="Game info">Game info</a></li>
					<li><a href="<?php echo URL;?>help" class="faq"  title="FAQ">FAQ</a></li>
				</ul>
			</nav>
			<div id="profile">
				<div class="char_info">
				<?php if (!is_logged_in()) {
					echo "You're not logged in. Login or Register to start playing.";
				} else if (!isset($_SESSION['character'])) {
					echo "You haven't selected a character yet. Create one <a href=\"".URL."character\">here</a>";
				} else {
					echo "You're playing as " . $_SESSION['character'];
				}
				?>
				<p><a href="<?php echo URL . "login/logout"; ?>">Logout</a></p>
				</div>
				<?php if (isset($_SESSION['gender']) && $_SESSION['gender'] == 'f') { ?>
				<div id="<?php echo $_SESSION['char_race']; ?>" class="race female"></div>
				<?php } else { ?>
				<div id="<?php echo $_SESSION['char_race']; ?>" class="race"></div>
				<?php } ?>
				<div id="profile_options">
					<ul id="options">
						<li><a href="<?php echo URL;?>profile">Manage profile</a></li>
						<li><a href="<?php echo URL;?>character">Create a new Character</a></li>
					</ul>
				</div>
			</div>
		</header>
	</div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
	//$( "#profile_options" ).menu();
    $('#profile').bind('contextmenu', function(event) {
    	event.preventDefault();
		$("#profile_options").fadeIn(500, startFocusOut());
    });
    $('#profile').bind('contextmenu', function(event) {
    	event.preventDefault();
    });
});
function startFocusOut() {
    $(document).on("click", function () {   
        $("#profile_options").hide(500);
        $(document).off("click");           
    });
}
</script>