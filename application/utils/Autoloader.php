<?php
spl_autoload_register('Autoloader::ClassLoader');
spl_autoload_register('Autoloader::UtilsLoader');
spl_autoload_register('Autoloader::ModelLoader');
spl_autoload_register('Autoloader::ViewLoader');
spl_autoload_register('Autoloader::ControllerLoader');

class Autoloader
{
	public static function ClassLoader($className)
	{
		if (file_exists('application/' . $className . '.php'))
			include_once 'application/' . $className . '.php';
	}

	public static function UtilsLoader($className)
	{
		if (file_exists('application/utils/' . $className . '.php'))
			include_once 'application/utils/' . $className . '.php';
	}
	
	public static function ModelLoader($className)
	{
		if (file_exists('application/model/' . $className . '.php'))
			include_once 'application/model/' . $className . '.php';
	}
	
	public static function ViewLoader($className)
	{
		if (file_exists('application/view/' . $className . '.php'))
			include_once 'application/view/' . $className . '.php';
	}
	
	public static function ControllerLoader($className)
	{
		if (file_exists('application/controller/' . $className . '.php'))
			include_once 'application/controller/' . $className . '.php';
	}
}