<?php
class PlayModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function init()
	{
		$user_id = $_SESSION['user_id'];
		$this->db->prepare("SELECT m.id, u.username FROM maps m JOIN users u ON m.user_id=u.id");
		$this->db->execute();
		$data = $this->db->getResult();
		$content = $data;
		return $content;
	}
	
	/**
	 * getMap
	 * retrieves the requested map and current character data from the database
	 * @param int $map
	 * @return array
	 * @todo move character data in another function
	 */
	public function getMap($map)
	{
		try {
			$user_id = $_SESSION['user_id'];
			$this->db->prepare("SELECT map_data FROM maps WHERE id='$map'");
			$this->db->execute();
			$data = $this->db->getResult();
			$data['map_data'] = $data[0]['map_data'];
			
			$this->db->prepare("SELECT c.name, c.exp, c.gender, r.*, cl.class_name FROM classes cl
					JOIN characters c ON c.race_id=cl.id
					JOIN races r ON c.race_id=r.id WHERE user_id=$user_id AND selected=1");
			$this->db->execute();
			$data['character'] = $this->db->getResult();
			return $data;
		} catch (Exception $e) {
			//log this exception
			header("location: ".URL."/character");
		}
	}
}