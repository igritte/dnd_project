<?php
class Registry
{
	private $variables;
	
	public function __get($key)
	{
		if (array_key_exists($key, $this->variables)) {
			return $this->variables[$key];
		}
		
		return null;
	}
	
	public function __set($key, $value)
	{
		$this->variables[$key] = $value;
	}
}