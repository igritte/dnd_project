<?php
class MapeditorModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * save
	 * saves the map created by the user in the database
	 * @param JSON $map
	 * @todo return true/false depending on whether the operaion was successful
	 * @todo fix the problem with circular JSON structure 
	 */
	public function save($map)
	{
		$this->db->prepare("INSERT INTO maps(user_id, map_data) VALUES(:user_id, :map_data)");
		$this->db->bind(":user_id", $_SESSION['user_id']);
		$this->db->bind(":map_data", $map);
		$this->db->execute();
	}
	
	/**
	 * edit
	 * loads the requested map JSON from the database
	 * @param int $map
	 * @return JSON
	 */
	public function edit($map)
	{
		$this->db->prepare("SELECT map_data FROM maps WHERE id='$map'");
		$this->db->execute();
		$data = $this->db->getResult();
		return $data[0];
	}
}