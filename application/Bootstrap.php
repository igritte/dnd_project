<?php
class Bootstrap {
	
	private $controller = null;
	private $action = null;
	private $args = null;
	private $controllerPath = 'application/controller/';
	private $modelPath = 'application/model/';
	
	public function __construct()
	{
		
	}
	
	public function run()
	{
		$this->splitUrl();
		
		if (!isset($this->controller) || empty($this->controller)) {
			$this->loadDefault();
			return false;
		}
		
		$this->loadController();
		
		if (isset($this->action) && !empty($this->action)) {
			$this->executeAction();
		}
		else
			$this->controller->index();
	}
	
	public function splitUrl()
	{
		if (isset($_GET['url'])) {
			$url = rtrim($_GET['url'], '/');
			$url = filter_var($url, FILTER_SANITIZE_URL);
			$url = explode('/' , $url);
			
			$this->controller = (isset($url[0]) && !empty($url[0])) ? ucfirst($url[0]) : null;
			$this->action = (isset($url[1]) && !empty($url[1])) ? $url[1] : null;
			
			for ($i = 2; $i < count($url); $i++) {
					$this->args[] = $url[$i];
			}
		}
	}
	
	public function loadController()
	{
		$file = $this->controllerPath . $this->controller . '.php';
		if (file_exists($file)) {
			$modelName = $this->controller;
			$this->controller = new $this->controller;
			$this->controller->loadModel($modelName, $this->modelPath);
		} else {
			$this->loadDefault();
			return false;
		}
	}
	
	public function executeAction()
	{
		if (method_exists($this->controller, $this->action)) {
			if (isset($this->args) && !empty($this->args)) {
				call_user_func_array(array($this->controller, $this->action), $this->args);
			} else {
				$this->controller->{$this->action}();
			}
		} else {
			$this->loadController();
		}
	}
	
	public function loadDefault()
	{
		$this->controller = new Login();
		$this->controller->index();
	}
}