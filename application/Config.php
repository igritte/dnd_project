<?php
/**
 * static class that handles reading the config ini file
 */
class Config
{
	private static $instance;
	public $properties = array();
	
	private function __construct()
	{
		$this->properties = parse_ini_file("config/config.ini");
	}
	
	private function __clone(){}
	
	public static function getInstance() 
	{
		if(empty(self::$instance))
		{
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public static function getAttribute($key)
	{
		if(isset(self::getInstance()->properties[$key])) {
			return self::getInstance()->properties[$key];
		} else {
			return false;
		}
	}
	
	public function __get($name)
	{
		return self::getAttribute($name);
	}
}
