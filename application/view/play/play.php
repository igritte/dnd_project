<style>
#canvas {
	width: 544px;
	height: 352px;
	position: absolute;
	left: 15%;
}
#log {
	width: 200px;
	height: 200px;
	color: white;
}
#map, #foreground {
	position: absolute;
}

</style>
<div id="canvas">
	<canvas id="map" width="544" height="352"></canvas>
	<canvas id="foreground" width="544" height="352"></canvas>
</div>
	<div id="log"></div>
	<button id="pass_turn">Pass Turn</button>
<?php print_r($this->content[0]); ?>
<script>
require(["js/map", "js/camera","js/game", "js/character", "js/mob", "js/scene"], function(Map, Camera, Game, Character, Mob, Scene) {
	var canvas = document.getElementById('map');
	var mapData = JSON.parse(<?php echo json_encode($this->content['map_data']); ?>);
	var map = new Map(mapData, canvas, 1);
	var character = <?php echo json_encode($this->content['character'][0]); ?>;
	var mob = <?php echo json_encode($this->content['character'][0]); ?>;
	console.log(character);
	var sprite = new Image();
	sprite.src = 'src/tokens/male_dragonborn.png';
	var sprite2 = new Image();
	sprite2.src = 'src/tokens/male_dwarf.png';
	var tilesetImage = new Image(); //the tile image
	tilesetImage.src = 'src/characters.png';
	var array = [];
	for (var i = 0; i < 1; i++) {
		array.push([]);
		for (var j = 0; j < map.rows; j++) {
			array[i].push([]);
			for (var int2 = 0; int2 < map.cols; int2++) {
				array[i][j].push(0);
			}
		}
	}
	var mapJSON = {	'rows' : map.rows,
					'cols' : map.cols,
					'layers' : array,
					'tilesize' : map.tilesize,
					'imageNumTiles' : map.imageNumTiles,
					'tilesetImage' : tilesetImage,
					'scale' : 1};
	
	var charCanvas = document.getElementById('foreground');
	var scene = new Scene(mapJSON, charCanvas, 1);
	var player = new Character(character, sprite, charCanvas);
	game = new Game(player, map, scene);
	var player2 = new Mob(mob, sprite2, charCanvas);
	var player3 = new Mob(mob, sprite2, charCanvas);
	player2.x = 672;
	player2.y = 64;
	player2.name = 'gad1';
	game.addMob(player2);
	player3.x = 544;
	player3.y = 64;
	player3.name = 'gad2';
	game.addMob(player3);
	sprite.onload = function(){
		game.log = $('#log');
		game.start();
		console.log(game.scene.layers);
		game.map.render();
		game.scene.render();	
	};
	console.log(game.map.cols * game.map.tilesize, game.map.rows * game.map.tilesize);
	console.log(game.scene.cols * game.scene.tilesize, game.scene.rows * game.scene.tilesize);
	$('#minimap').on('click', function(event) {
		ctx = $('#map')[0].getContext('2d'),
		pos = editor.camera.getPosition(),
		scale = editor.minimap.getScale(),
		normalized = editor.camera.normalizeCoords(x, y, scale),
		realCoords = editor.camera.checkCoords(normalized.x, normalized.y);
		console.log(x, y, editor.minimap.getScale());
		
		console.log('pos' + pos.x + ' ' + pos.y);
		ctx.translate(pos.x - realCoords.x, pos.y - realCoords.y);
		editor.camera.setPosition(realCoords.x, realCoords.y, 1);
		console.log('bounds' + editor.camera.isGridInBounds(x, y, scale));
    	editor.map.render();
	});
	
	$(document).on('keyup', function(e){
		if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
			e.stopPropagation();
		    var	tilesize = game.map.getTilesize(),
				pos = game.camera.getPosition(),
				transX,transY;

			console.log(e.keyCode);
		    if (e.keyCode == 40) {
		    	// down arrow
		    	transX = 0;
		    	transY = -tilesize;
		    } else if (e.keyCode == 39) {
		    	// right arrow
		    	transX = -tilesize;
		    	transY = 0;
		    } else if (e.keyCode == 37) {
		    	// left arrow
		    	transX = tilesize;
		    	transY = 0;
		    } else if (e.keyCode == 38) {
		    	// up arrow
		    	transX = 0;
		    	transY = tilesize;
		    }
			console.log('vhod' + (game.player.x - transX)/32 + ' ' + (game.player.y - transY)/32);

			if (game.combat == false) {
				game.movePlayer(game.player.x - transX, game.player.y - transY);
			    game.cameraFollow(pos.x, pos.y);
			    game.visibleEnemies();
			} else if (game.movesLeft > 0 && game.turnOwner == game.player) {
				game.movePlayer(game.player.x - transX, game.player.y - transY);
			}
	    	return false;
	    }
	});

	$('#pass_turn').on('click', function() {
		game.switchTurn();
		console.log(game.turnOwner);
	});
});
</script>