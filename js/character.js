/**
 * 
 */
define(["js/lib/class"], function() {
	
var Character = Class.extend({
	init: function(obj, sprite, scene)
	{
		console.log(obj.name);
		this.x = 0;
		this.y = 0;
		
		this.name = obj.name;
		this.class_name = obj.class_name;
		this.race = obj.race_name;
		this.gender = obj.gender;
		this.exp = obj.exp;
		
		//this.orientation = 'DOWN';
		this.target = null;
		this.attackers = null;
		
		this.HP = 200;
		this.maxHP = 200;
		this.sprite = sprite;
		this.scene = scene;
		this.tilesize = 32;
		
		this.speed = 6;
		this.initiative = 0;
		this.token = obj.token;
		this.alive = true;
		console.log(this);
	},
	
	setPosition: function(x, y)
	{
		this.x = x;
		this.y = y;
	},
	
	moveTo: function(x, y)
	{
		var ctx = this.scene.getContext("2d");
		console.log('delete' + this.x/32 + ' ' + this.y/32);
		//ctx.clearRect(this.x, this.y, this.tilesize, this.tilesize);
		//this.draw(x, y);
		console.log('move ' + x/32 + ' ' + y/32);
		this.setPosition(x, y);
	},
	
	start: function()
	{
		var ctx = this.scene.getContext("2d");
		ctx.drawImage(this.sprite, this.x, this.y, this.tilesize, this.tilesize);
	},
	
	draw: function(x, y)
	{
		var ctx = this.scene.getContext("2d");
		ctx.drawImage(this.sprite, x, y, this.tilesize, this.tilesize);
	},
	
	attack: function()
	{
		var amount = Math.floor((Math.random() * 21) + 20);
		if (this.target != null) {
			this.target.takeDmg(amount);
			game.log.append('<p class="dmg">' + this.name + ' attacked ' + this.target.name + ' for ' + amount + '.</p>');
		}
	},
	
	heal: function() {
		var amount = Math.floor((Math.random() * 10) + 21); 
		this.HP += amount;
		this.HP = Math.min(this.HP, this.maxHP);
		$('#hp').width( 100 * (this.HP / this.maxHP) );
		$('#log').append('<p class="heal">' + this.name + ' was healed for ' + amount + '.</p>');
	},
	
	takeDmg: function(amount)
	{
		this.HP -= amount;
		this.HP = Math.max(this.HP, 0);
		$('#hp').width( 100 * (this.HP / this.maxHP) );
		if (this.HP <= 0) {
			this.die();
		}
	},
	
	die: function() {
		this.alive = false;
	}
	
});

	return Character;
});