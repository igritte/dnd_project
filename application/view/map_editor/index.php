<div id="selector"></div>
	<div id="selector2"></div>
	<div id="main">
		<div class="page">
				<div id="canvas">
					<canvas id="map" width="512" height="320"></canvas>
				</div>
			<div id="editor">
			<div class="minimap"><p>Minimap</p></div>
				<canvas id="minimap" width="512" height="320"></canvas>
					<div id="layers">
						<button id="add" value="add">Add</button>
					</div>
					<img id="tileset" alt="" src="src/tile3.png">
				</div>
				<button id="clear"></button>
				<div id="save_button"></div>
				<span id="msg"></span>
			</div>
		</div>
	</div>
</body>
<?php if (isset($_SESSION['editing']) && isset($this->content) && !empty($this->content)) { ?>
<script type="text/javascript">
require(["js/map", "js/map_editor", "js/camera"], function(Map, MapEditor, Camera) {

var mapData = JSON.parse(<?php echo json_encode($this->content['map_data']); ?>);

mapJSON = {	'rows' : mapData.rows,
		'cols' : mapData.cols,
		'layers' : mapData.layers,
		'tilesize' : mapData.tilesize,
		'imageNumTiles' : mapData.imageNumTiles,
		'tilesetImage' : mapData.tilesetImage,
		'scale' : 1};

});
</script>
<script src="js/edit_map.js"></script>
<?php } else { ?>
<script src="js/map_editor_app.js"></script>
<?php }?>
<script>
var layer = 0;
var selected_layer = '';
$( document ).ready(function() {
    $('#avatar').bind("contextmenu", function(event) {
    	event.preventDefault();
		$("#profile_options").fadeIn(500, startFocusOut());
    });
    var map = document.getElementById('map');
    var ctx = map.getContext('2d');
    //cloneCanvas(map);
    //minimap.drawImage(map, 0, 0);
    
    $(function() {
	    $( "#tabs" ).tabs({
	    beforeLoad: function( event, ui ) {
	    ui.jqXHR.error(function() {
	    ui.panel.html(
	    "Couldn't load this tab." );
	    	});
    	}
		});
   });
   $('#clear').on('click', function() {
	   request = $.ajax({
			type: "POST",
			url: document.URL + "/clear",
			success: function(response){
				$('#msg').html(response.msg);
				location.reload(); 
			}
			});
   });
});

function startFocusOut() {
    $(document).on("click", function () {   
        $("#profile_options").hide(500);
        $(document).off("click");           
    });
}
</script>