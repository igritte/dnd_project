<?php
class Dungeon extends BaseController
{
	function __construct()
	{
		Auth::isUserOnline();
		parent::__construct();
	}

	public function index()
	{
		$this->view->content = $this->model->getMaps();
		$this->view->render('dungeons/index');
	}
}