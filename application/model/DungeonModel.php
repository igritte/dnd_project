<?php
class DungeonModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * getMaps
	 * retrieves all user maps from the database
	 * @return multitype:
	 */
	public function getMaps()
	{
		$this->db->prepare("SELECT m.id, u.username FROM maps m JOIN users u ON m.user_id=u.id");
		$this->db->execute();
		$data = $this->db->getResult();
		$content = $data;
		return $content;
	}
}