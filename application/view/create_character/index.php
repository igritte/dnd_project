<div id="main">
<div class="page">
	<div class="content">
		<form id="character_creation" action="<?php echo URL;?>character/create">
		<label style="color: #19232C;font-size: 24px;">Name: </label><input id="name" name="name"/><span id="name_error"></span>
		<div id="accordion">
		<h3 class="picker">Choose a gender</h3>
		<div id="gender">
			<label>Male </label><input type="radio" name="gender" value="male"/>
			<label>Female </label><input type="radio" name="gender" value="female"/>
		</div>
		<h3 class="picker">Choose a race</h3>
		<div id="race">
			<?php //get_race_selection(); 
				foreach ($this->content["race"] as $key=>$value) {
					echo "<h3>" . $value["race_name"] . "</h3>";
					echo "<div class=\"race_img\" id=\"" . $value["race_name"] . "\"></div>";
					echo "<div><p>" . $value["description"] . "</p></div>";
				}
			?>
		</div>
		<h3 class="picker">Choose a class</h3>
		<div id="class">
			<?php //get_class_selection();
				foreach ($this->content["class"] as $key=>$value) {
					echo "<h3 class=\"class_picker\" id=\"" . $value["class_name"] . "\">" . $value["class_name"] . "</h3>";
					echo "<div><p>" . $value["description"] . "</p></div>";
				}
			?>
		</div>
		<h3 class="picker" id="stat_picker">Choose your stats</h3>
		<div id="stats">
			<div class="stat_picker">
			Free points: <div id="free_stats">20</div>
			<?php $stats = $this->content['stat']; 
				foreach ($stats as $stat => $value) {
					echo "<div class=\"stats\">$value<span class=\"minus\">-</span><span id=\"$stat\">10</span><span class=\"plus\">+</span></div>";
				}
			?>
			</div>
		</div>
		<!-- 
		<h3 class="picker" id="skills_picker">Choose your skills</h3>
			<div id="skills">
				<select id="options">
				</select>
			</div>
		 -->
		</div>
		<span id="submit_error"></span><input type="submit" id="save" value="" name="create"/>
		</form>
		</div>
	</div>
	</div>
<script>console.log($('#main').width());</script>
<script>
$(document).ready(function() {
	$( "#accordion" ).accordion({
		heightStyle: "content",
		collapsible: true
	});
var content = <?php echo json_encode($this->content); ?>;
console.log(content);
var race = '',
	gender = '',
	selectedClass = '',
	selectedSkills = 3;
	freeStats = 20;


    $('#avatar').bind('contextmenu', function(event) {
    	event.preventDefault();
		$("#profile_options").fadeIn(500, startFocusOut());
    });
    
    $('#profile').bind('contextmenu', function(event) {
    	event.preventDefault();
    });
    
    $('.picker').bind('click', function(event) {
    	console.log($(this).children());
    });
    
    $('.minus').bind('click', function(event) {
    	var value = parseInt($(this).next().html());
    	if (value > 8) {
    		decrease(value, $(this).next());
    		$('#free_stats').html(freeStats);
    	}
    });
    
    $('.plus').bind('click', function(event) {
    	var value = parseInt($(this).prev().html());
    	increase(value, $(this).prev());
    	$('#free_stats').html(freeStats);
    });
    
    $('.race_img').bind('click', function(event) {
    	if (race != '') {
     		$('#'+race).removeClass('chosen');
     	}
     	if (race == 'human') {
     		$('#bonus').show();
     	} else {
			$('#bonus').hide();
     	}
    	race = this.id;
    	$(this).addClass(' chosen');
    	console.log(race);
    	$.ajax({
	       url: document.URL + "/chooseRace",
	       data: {'selectedRace' : race},
	       type: "POST",
	       dataType: "json",
	       success: function(response) {
		       console.log(response);
			}
		});
    });
    
     $('.class_picker').bind('click', function(event) {
     	if (selectedClass != '') {
     		$('#'+selectedClass).removeClass('chosen');
     	}
     	$(this).addClass(' chosen');
    	selectedClass = this.id;
    	$.ajax({
		       url: document.URL + "/chooseClass",
		       data: {'selectedClass' : selectedClass},
		       type: "POST",
		       dataType: "json",
		       success: function(response) {
			       console.log(response);
			}
		});
    	    var options = $("#options");
    	    //don't forget error handling!
    	    $.each(content.skills, function(item) {
    	        options.append($("<option />").val(item.selectedClass).text(this));
    	    });
    	});
     
    $('#gender input').click(function() {
    	gender = $(this).val();
    	if (gender == 'female')
    		$('.race_img').addClass(gender);
    	else
    		$('.race_img').removeClass('female');
    });
    
    $('#character_creation').on("submit", function(event){
		event.preventDefault();
		$('#submit_error').removeClass('error').html('');
		   $.ajax({
		       url: $('#character_creation').attr("action"),
		       data: $('#character_creation').serialize(),
		       type: "POST",
		       dataType: "json",
		       success: function(response) {
					if (response.status === 'fail') {
						$('#submit_error').addClass('error').html(response.msg);
					} else {
						$('#submit_error').addClass('error').html(response.msg);
					}
				}
		  });
	});
    
    $('#skill_picker').on('click', function() {
		if (selectedClass != '') {
			request = $.ajax({
				type: "POST",
				url: 'helper.php',
				data: {'skills': selectedClass},
				success: function(response){
					response = JSON.parse(response);
					response = response.selectedClass;
					console.log(response);
					if (response != '') {
						$.each(response, function(i, val) {
							console.log(i, val);
							//$('#skills').append('<div id="' + this + '"></div>');
						});
					}
		        }
			});
		}
    });
});

function startFocusOut() {
    $(document).on("click", function () {   
        $("#profile_options").hide(500);              // To hide the context menu
        $(document).off("click");           
    });
}

function increase(value, element) {
	value += 1;
	if (value >= 10 && value < 14) {
		if (freeStats >= 1) {
			freeStats -= 1;
			element.html(value);
		}
	} else if (value >= 14 && value < 17) {
		if (freeStats >= 2) {
			freeStats -= 2;
			element.html(value);
		}
	} else if (value == 17) {
		if (freeStats >= 3) {
			freeStats -= 3;
			element.html(value);
		}
	} else if (value >= 18) {
		if (freeStats >= 4) {
			freeStats -= 4;
			element.html(value);
		}
	}
}

function decrease(value, element) {
	value -= 1;
	if (value >= 8 && value < 14) {
		freeStats += 1;
		element.html(value);
	} else if (value >= 8 && value < 14) {
		freeStats += 1;
		element.html(value);
	} else if (value >= 14 && value < 17) {
		freeStats += 2;
		element.html(value);
	} else if (value == 17) {
		freeStats += 3;
		element.html(value);
	} else if (value >= 18) {
		freeStats += 4;
		element.html(value);
	}
}
</script>