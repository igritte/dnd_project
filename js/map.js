/*
 * 
 */

define(["js/lib/class"], function() {
	
	var Map = Class.extend({
		init: function(obj, scene, scale) {
			this.scene = scene;
			this.rows = obj.rows || 20;
			this.cols = obj.cols || 32;
			this.layers = obj.layers;
			this.tilesize = obj.tilesize || 32;
			this.imageNumTiles = obj.imageNumTiles || 16;
			if (typeof tilesetImage === 'undefined') {
				this.tilesetImage = new Image();
				this.tilesetImage.src = 'http://localhost/kurs/DnD/src/tile3.png';
			} else {
				this.tilesetImage = obj.tilesetImage;
			}
			this.scale = scale || 1;
		},
		
		render: function() {
			var ctx = this.scene.getContext('2d');
			var tilesize = this.tilesize;
			if (this.scale != 1)
				ctx.scale(this.scale, this.scale);
			for (var r = 0; r < this.rows; r++) {
				for (var c = 0; c < this.cols; c++) {	
					for (var index = 0; index < this.layers.length; index++) {
						var tile = this.layers[index][ r ][ c ],
							tileRow = (tile / this.imageNumTiles) | 0,
							tileCol = (tile % this.imageNumTiles) | 0;
						ctx.drawImage(this.tilesetImage, (tileCol * tilesize),
									(tileRow * tilesize), tilesize, tilesize,
									(c * tilesize), (r * tilesize), tilesize, tilesize);
					}
				}
			}
		},
		
		renderTile: function(row, col)	{
			var tilesize = this.tilesize;
			var ctx = this.scene.getContext('2d');
			ctx.clearRect(col * tilesize, row * tilesize, tilesize, tilesize);
			for (var index = 0; index < this.layers.length; index++) {
				var tile = this.layers[index][row][col];
				var tileRow = (tile / this.imageNumTiles) | 0;
				var tileCol = (tile % this.imageNumTiles) | 0;
				ctx.drawImage(this.tilesetImage, (tileCol * this.tilesize),
							(tileRow * this.tilesize), this.tilesize, this.tilesize,
							(col * this.tilesize), (row * this.tilesize), this.tilesize, this.tilesize);
			}
		},
		
		getTile: function(x, y) {
			var row = (y / this.tilesize) | 0;
			var col = (x / this.tilesize) | 0;
			return {
		        row: row,
		        col: col
		    };  
		},
		
		setTile: function(row, col, layer, selected) {
			this.layers[layer][row][col] = selected;
		},
		
		addLayer: function(layer) {
			if (typeof layer === 'undefined') {
				var layer = [];
				for (var index = 0; index < this.cols; index++) {
					layer.push([]);
				}
			}
			this.layers.push(layer);
		},
		
		getLayersNum: function() {
			return this.layers.length;
		},
		
		getScale: function() {
			return this.scale;
		},
		
		getHeight: function() {
			return this.rows * this.tilesize;
		},
		
		getWidth: function() {
			return this.cols * this.tilesize;
		},

		getTilesize: function() {
			return this.tilesize;
		}
	});
		/*
		addClickHandler: function() {
			cols = this.cols;
			tilesize = this.tilesize;
			map = this;
			this.scene.addEventListener('click', function(event) {
				console.log("select" + select);
				var bounding = this.getBoundingClientRect();
				var left = bounding.left;
				var top = bounding.top;
				var x = event.pageX - left - document.body.scrollLeft;
				var y = event.pageY - top - document.body.scrollTop;
				var row = floor(y / tilesize);
				var col = floor(x / tilesize);
				console.log(this.id + ' - x: ' + x + ', y: ' + y);
				console.log(this.id + '' + x + ', y: ' + y);
				lastLayer = map.layers[0];
				for (var int = 0; int < map.layers.length; int++) {
					if (map.layers[int][row][col] != '') {
						lastLayer = map.layers[int];
					}
				}
				
					map.layers[0][row][col] = select;
					map.renderTile(row, col);
				
				return floor(this.x / 32) + floor(this.y / 32) * 16;
			}, false);
		},
		
		addRightClickHandler: function() {
			tilesize = this.tilesize;
			map = this;
			leftButtonDown = false;
			this.scene.addEventListener('contextmenu', function(event) {
				event.preventDefault();
				var bounding = this.getBoundingClientRect();
				var left = bounding.left;
				var top = bounding.top;
				var x = event.pageX - left - document.body.scrollLeft;
				var y = event.pageY - top - document.body.scrollTop;
				var row = floor(y / tilesize);
				var col = floor(x / tilesize);
				console.log(this.id + ' - x: ' + x + ', y: ' + y);
				lastLayer = map.layers[0];
				for (var int = 0; int < map.layers.length; int++) {
					console.log(map.layers[int][row][col]);
					if (map.layers[int][row][col] !== undefined) {
						console.log('?');
						lastLayer = map.layers[int];
					}
				}
				//lastLayer = map.layers[map.layers.length-1];
				console.log('layer ' + map.layers.indexOf(lastLayer) + ', tile at: ' + lastLayer[row][col]);
				
					console.log('layer ' + lastLayer[row][col]);
					lastLayer[row][col] = undefined;
					console.log(lastLayer);
						var start = ((x / map.tilesize) | 0) * tilesize;
						var end = ((y / map.tilesize) | 0) * tilesize;
						console.log('start, end: ' + start, end);
						map.scene.getContext('2d').clearRect(start, end, tilesize, tilesize);
					
					map.renderTile(row, col);
				
			}, false);
		},
		
		addDragHandler: function(tiles) {
			cols = this.cols;
			tilesize = this.tilesize;
			map = this;
			this.scene.addEventListener('mousemove', drag_hangler=function(event) {
				if (leftButtonDown) {
					var bounding = this.getBoundingClientRect();
					var left = bounding.left;
					var top = bounding.top;
					var x = event.pageX - left - document.body.scrollLeft;
					var y = event.pageY - top - document.body.scrollTop;
					var row = floor(y / tilesize);
					var col = floor(x / tilesize);
					var tile = row + col * 16;
					if (tiles.indexOf(tile) == -1) {
						tiles.push(tile);
					}
					if (select != 0){
						lastLayer = map.layers[map.layers.length-1];
						lastLayer[row][col] = select;
						map.renderTile(row, col);
					}
				}
				return tiles;
			}, false);
		},
		
		addMouseDownHandler: function(select) {
			cols = this.cols;
			tilesize = this.tilesize;
			map = this;
			select = select || null;
			tiles = [];
			this.scene.addEventListener('mousedown', function(event) {
				event = event || window.event;
			    var button = (typeof event.which != "undefined") ? event.which : event.button;
			    if (button != 3) {
					leftButtonDown = true;
			    }
				var bounding = this.getBoundingClientRect();
				var left = bounding.left;
				var top = bounding.top;
				var x = event.pageX - left - document.body.scrollLeft;
				var y = event.pageY - top - document.body.scrollTop;
				map.addDragHandler(tiles);
			}, false);
		},
	
		addMouseUpHandler: function(select) {
			map = this;
			leftButtonDown = false;
			this.scene.addEventListener('mouseup', function(event) {
				map.scene.removeEventListener('mousemove', drag_hangler);
				console.log(tiles);
			}, false);
		}
	});
/*
	function(layers, tilesize, rows, cols, imageNumTiles, tilesetImage, scene) {
	this.layers = layers; //optional, leave empty if there're no objects above ground level
	this.tilesize = tilesize;
	this.rows = rows;
	this.cols = cols;
	this.imageNumTiles = imageNumTiles;
	this.tilesetImage = tilesetImage;
	this.scene = scene;
};

//Map's rendering method
Map.prototype.render = function(ctx) {
	for (var r = 0; r < this.rows; r++) {
		for (var c = 0; c < this.cols; c++) {	
			for (var index = 0; index < this.layers.length; index++) {
				var tile = this.layers[index][ r ][ c ];
				var tileRow = (tile / this.imageNumTiles) | 0;
				var tileCol = (tile % this.imageNumTiles) | 0;
				ctx.drawImage(this.tilesetImage, (tileCol * this.tilesize), (tileRow * this.tilesize), this.tilesize, this.tilesize, (c * this.tilesize), (r * this.tilesize), this.tilesize, this.tilesize);
			}
		}
	}
};

Map.prototype.renderTile = function(ctx, tile, row, col)
{
	tilesize = this.tilesize;
	var tileRow = tile != 0 ? ((tile / this.imageNumTiles) | 0) : 0;
	var tileCol = tile != 0 ? ((tile % this.imageNumTiles) | 0) : 0;
	console.log(tileRow, tileCol);
	ctx.drawImage(this.tilesetImage, (tileCol * tilesize), (tileRow * tilesize), tilesize, tilesize, (col * tilesize), (row * tilesize), tilesize, tilesize);
};

Map.prototype.addClickHandler = function(selected) {
	cols = this.cols;
	tilesize = this.tilesize;
	map = this;
	select = select || null;
	this.scene.addEventListener('click', function(event) {
		var bounding = this.getBoundingClientRect();
		var left = bounding.left;
		var top = bounding.top;
		var x = event.pageX - left - document.body.scrollLeft;
		var y = event.pageY - top - document.body.scrollTop;
		var row = floor(y / tilesize);
		var col = floor(x / tilesize);
		console.log(this.id + ' - x: ' + x + ', y: ' + y);
		lastLayer = map.layers[0];
		for (var int = 0; int < map.layers.length; int++) {
			if (map.layers[int][row][col] != '') {
				lastLayer = map.layers[int];
			}
		}
		if (select != 0){
			lastLayer = map.layers[map.layers.length-1];
			lastLayer[row][col] = select;
			map.renderTile(ctx, select, row, col);
		}
		return floor(this.x / 32) + floor(this.y / 32) * 16;
	}, false);
};

Map.prototype.addRightClickHandler = function() {
	tilesize = this.tilesize;
	map = this;
	leftButtonDown = false;
	this.scene.addEventListener('contextmenu', function(event) {
		event.preventDefault();
		var bounding = this.getBoundingClientRect();
		var left = bounding.left;
		var top = bounding.top;
		var x = event.pageX - left - document.body.scrollLeft;
		var y = event.pageY - top - document.body.scrollTop;
		var row = floor(y / tilesize);
		var col = floor(x / tilesize);
		console.log(this.id + ' - x: ' + x + ', y: ' + y);
		lastLayer = map.layers[map.layers.length-1];
		for (var int = 0; int < map.layers.length; int++) {
			console.log(map.layers[int][row][col]);
			if (map.layers[int][row][col] !== undefined) {
				console.log('?');
				lastLayer = map.layers[int];
			}
		}
		//lastLayer = map.layers[map.layers.length-1];
		console.log('layer ' + map.layers.indexOf(lastLayer) + ', ' + lastLayer[row][col]);
		if (lastLayer[row][col] != 0) {
			console.log(lastLayer[row][col]);
			lastLayer[row][col] = 0;
			console.log(lastLayer);
			map.render(ctx);
		}
	}, false);
};

Map.prototype.addDragHandler = function(tiles) {
	cols = this.cols;
	tilesize = this.tilesize;
	map = this;
	select = select || null;
	this.scene.addEventListener('mousemove', drag_hangler=function(event) {
		if (leftButtonDown) {
			var bounding = this.getBoundingClientRect();
			var left = bounding.left;
			var top = bounding.top;
			var x = event.pageX - left - document.body.scrollLeft;
			var y = event.pageY - top - document.body.scrollTop;
			var row = floor(y / tilesize);
			var col = floor(x / tilesize);
			var tile = row + col * 16;
			if (tiles.indexOf(tile) == -1) {
				tiles.push(tile);
			}
			if (select != 0){
				lastLayer = map.layers[map.layers.length-1];
				lastLayer[row][col] = select;
				map.renderTile(ctx, select, row, col);
			}
		}
		return tiles;
	}, false);
};

Map.prototype.addMouseDownHandler = function(selected) {
	cols = this.cols;
	tilesize = this.tilesize;
	map = this;
	select = select || null;
	tiles = [];
	this.scene.addEventListener('mousedown', function(event) {
		event = event || window.event;
	    var button = (typeof event.which != "undefined") ? event.which : event.button;
	    if (button != 3) {
			leftButtonDown = true;
	    }
		var bounding = this.getBoundingClientRect();
		var left = bounding.left;
		var top = bounding.top;
		var x = event.pageX - left - document.body.scrollLeft;
		var y = event.pageY - top - document.body.scrollTop;
		map.addDragHandler(tiles);
	}, false);
};

Map.prototype.addMouseUpHandler = function(selected) {
	map = this;
	leftButtonDown = false;
	this.scene.addEventListener('mouseup', function(event) {
		map.scene.removeEventListener('mousemove', drag_hangler);
		console.log(tiles);
	}, false);
};
*/
	return Map;
});