<?php
/*
 * Simple email validator
 */
class EmailValidator extends Validator
{
	public function isValid($value)
	{
		$validate = filter_var($value, FILTER_VALIDATE_EMAIL);
		if ($validate) {
			return true;
		} else {
			$this->message = "Invalid email.";
			return false;
		}
	}
}