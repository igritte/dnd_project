<?php

class PasswordValidator extends Validator
{
	public function isValid($value)
	{
		if (preg_match('/^[\w-_*!.^()]{5,20}$/', $value)) {
			return true;
		} else {
			$this->message = "Password must be between 5 and 20 symbols and contain only letters, numbers and -_*!.^()";
			return false;
		}
	}
}