<div id="main">
<div class="page">
	<div class="content">
	<?php 
	foreach ($this->content as $key => $value) {
		echo "<div>Map " . $value['id'] . ", created by " . $value['username'];
		echo "<button class=\"map\" id=\"" . $value['id'] . "\"> Play </button>";
		if (isset($_SESSION['username']) && $_SESSION['username'] == $value['username']) {
			echo "<button class=\"edit_map\" id=\"" . $value['id'] . "\"> Edit </button>";
		}
		echo "</div>";
	}
	
	?>
	<span id="error"></span>
	</div>
</div>
</div>

<script>
	$(document).ready( function() {
		$('.map').on('click', function() {
			$.ajax({
			    url: "http://localhost/kurs/DnD/play/getMap",
			    data: {'map' : $(this).attr('id')},
			    type: "POST",
			    success: function(response) {
						if (response.status === 'fail') {
							$('#error').html(response.msg);
						} else {
							window.location.replace("http://localhost/kurs/DnD/play");
						}
					}
			});
		});
		
		$('.edit_map').on('click', function() {
			$.ajax({
			    url: "http://localhost/kurs/DnD/mapeditor/editMap",
			    data: {'edit' : $(this).attr('id')},
			    type: "POST",
			    success: function(response) {
						if (response.status === 'fail') {
							$('#error').html(response.msg);
						} else {
							window.location.replace("http://localhost/kurs/DnD/mapeditor");
						}
					}
			});
		});
	});
</script>