<?php
class Character extends BaseController
{
	function __construct()
	{
		Auth::isUserOnline();
		parent::__construct();
		$this->view->header = "header.php";
		
	}

	public function index()
	{
		$this->view->content = $this->model->init();
		$this->view->render('create_character/index');
	}
	
	public function chooseClass()
	{
		$_SESSION['class'] = $_POST['selectedClass'];
	}
	
	public function chooseRace()
	{
		$_SESSION['race'] = $_POST['selectedRace'];
	}
	
	public function create()
	{
		if (isset($_POST['name']) && !empty($_POST['name']) && isset($_POST['gender']) && isset($_SESSION['race']) && isset($_SESSION['class'])) {
			$name = $_POST['name'];
			$gender = $_POST['gender'];
			echo $name . $_SESSION["user_id"]. $_SESSION['race']. $_SESSION['class']. '0' . $gender[0];
			if (!$this->model->create($name, $_SESSION["user_id"], $_SESSION['race'], $_SESSION['class'], 0, $gender[0])) {
				echo json_encode( array( "status" => "fail", "msg" => "An error occurred."));
			} else {
				echo json_encode( array( "status" => "success", "msg" => "Great! Now select your new character and start playing."));
			}
		} else {
			echo json_encode( array( "status" => "fail", "msg" => "Some fields are missing."));
		}
	}
}