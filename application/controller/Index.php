<?php
class Index extends BaseController
{
	function __construct()
	{
		Auth::isUserOnline();
		parent::__construct();
		$this->view->header = "header_front.php";
		$this->view->styles = "styles/styles-front.css";
		$this->view->menu = "menu_front.php";
	}
	
	public function index()
	{
		$this->view->render('index/index');
	}
}