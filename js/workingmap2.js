function init() {
	 var tileset = document.getElementById('tileset');
	 var editor = document.getElementById('editor');
	 }

//Define the map class 
var Map = function(layers, tileSize, rows, cols, imageNumTiles, tilesetImage, scene) {
	this.layers = layers; //optional, leave empty if there're no objects above ground level
	this.tileSize = tileSize;
	this.rows = rows;
	this.cols = cols;
	this.imageNumTiles = imageNumTiles;
	this.tilesetImage = tilesetImage;
	this.scene = scene;
};

//Map's rendering method
Map.prototype.render = function(ctx) {
	for (var r = 0; r < this.rows; r++) {
		for (var c = 0; c < this.cols; c++) {
			for (var index = 0; index < this.layers.length; index++) {
				var tile = this.layers[index][ r ][ c ];
				var tileRow = (tile / this.imageNumTiles) | 0; // Bitwise OR operation
				var tileCol = (tile % this.imageNumTiles) | 0;
				ctx.drawImage(this.tilesetImage, (tileCol * this.tileSize), (tileRow * this.tileSize), this.tileSize, this.tileSize, (c * this.tileSize), (r * this.tileSize), this.tileSize, this.tileSize);
			}
		}
	}
};

Map.prototype.render_tile = function(ctx, tile, row, col)
{
	tilesize = this.tileSize;
	var tileRow = tile != 0 ? ((tile / this.imageNumTiles) | 0) : 0; // Bitwise OR operation
	var tileCol = tile != 0 ? ((tile % this.imageNumTiles) | 0) : 0;
	console.log(tileRow, tileCol);
	ctx.drawImage(this.tilesetImage, (tileCol * tilesize), (tileRow * tilesize), tilesize, tilesize, (col * tilesize), (row * tilesize), tilesize, tilesize);
};

Map.prototype.addClickHandler = function(selected) {
	cols = this.cols;
	tileSize = this.tileSize;
	map = this;
	select = select || null;
	this.scene.addEventListener('click', function(event) {
		var bounding = this.getBoundingClientRect();
		var left = bounding.left;
		var top = bounding.top;
		var x = event.pageX - left - document.body.scrollLeft;
		var y = event.pageY - top - document.body.scrollTop;
		var row = Math.floor(y / tileSize);
		var col = Math.floor(x / tileSize);
		console.log(this.id + ' - x: ' + x + ', y: ' + y);
		lastLayer = map.layers[0];
		for (var int = 0; int < map.layers.length; int++) {
			if (map.layers[int][row][col] != '') {
				lastLayer = map.layers[int];
			}
		}
		if (select){
			lastLayer = map.layers[map.layers.length-1];
			lastLayer[row][col] = select;
			map.render_tile(ctx, select, row, col);
		}
		return Math.floor(this.x / 32) + Math.floor(this.y / 32) * 16;
	}, false);
};

Map.prototype.addRightClickHandler = function() {
	tileSize = this.tileSize;
	map = this;
	this.scene.addEventListener('contextmenu', function(event) {
		event.preventDefault();
		var bounding = this.getBoundingClientRect();
		var left = bounding.left;
		var top = bounding.top;
		var x = event.pageX - left - document.body.scrollLeft;
		var y = event.pageY - top - document.body.scrollTop;
		var row = Math.floor(y / tileSize);
		var col = Math.floor(x / tileSize);
		console.log(this.id + ' - x: ' + x + ', y: ' + y);
		lastLayer = map.layers[map.layers.length-1];
		for (var int = 0; int < map.layers.length; int++) {
			console.log(map.layers[int][row][col]);
			if (map.layers[int][row][col] !== undefined) {
				console.log('?');
				lastLayer = map.layers[int];
			}
		}
		//lastLayer = map.layers[map.layers.length-1];
		console.log('layer ' + map.layers.indexOf(lastLayer) + ', ' + lastLayer[row][col]);
		if (lastLayer[row][col] != 0) {
			console.log(lastLayer[row][col]);
			lastLayer[row][col] = 0;
			console.log(lastLayer);
			map.render(ctx);
		}
	}, false);
};

Map.prototype.addDragHandler = function(tiles) {
	cols = this.cols;
	tileSize = this.tileSize;
	map = this;
	select = select || null;
	this.scene.addEventListener('mousemove', drag_hangler=function(event) {
		var bounding = this.getBoundingClientRect();
		var left = bounding.left;
		var top = bounding.top;
		var x = event.pageX - left - document.body.scrollLeft;
		var y = event.pageY - top - document.body.scrollTop;
		var row = Math.floor(y / tileSize);
		var col = Math.floor(x / tileSize);
		var tile = row + col * 16;
		if (tiles.indexOf(tile) == -1) {
			tiles.push(tile);
		}
		if (select){
			lastLayer = map.layers[map.layers.length-1];
			lastLayer[row][col] = select;
			map.render_tile(ctx, select, row, col);
		}
		return tiles;
	}, false);
};

Map.prototype.addMouseDownHandler = function(selected) {
	cols = this.cols;
	tileSize = this.tileSize;
	map = this;
	select = select || null;
	tiles = [];
	this.scene.addEventListener('mousedown', function(event) {
		var bounding = this.getBoundingClientRect();
		var left = bounding.left;
		var top = bounding.top;
		var x = event.pageX - left - document.body.scrollLeft;
		var y = event.pageY - top - document.body.scrollTop;
		map.addDragHandler(tiles);
	}, false);
};

Map.prototype.addMouseUpHandler = function(selected) {
	map = this;
	this.scene.addEventListener('mouseup', function(event) {
		map.scene.removeEventListener('mousemove', drag_hangler);
		console.log(tiles);
	}, false);
};

var canvas = document.getElementById("main");
var ctx = canvas.getContext('2d');
var tileSize = 32;       // The size of a tile (32x32)
var rowTileCount = 20;   // The number of tiles in a row of our background
var colTileCount = 32;   // The number of tiles in a column of our background
var imageNumTiles = 16;  // The number of tiles per row in the tileset image


// The tileset arrays - 
var ground = [
    [172, 172, 172, 79, 34, 34, 34, 34, 34, 34, 34, 34, 56, 57, 54, 55, 56, 147, 67, 67, 68, 79, 79, 171, 172, 172, 173, 79, 79, 55, 55, 55],
    [172, 172, 172, 79, 34, 34, 34, 34, 34, 34, 146, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 155, 142, 172, 159, 189, 79, 79, 55, 55, 55],
    [172, 172, 172, 79, 79, 34, 34, 34, 34, 34, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 171, 172, 159, 189, 79, 79, 79, 55, 55, 55],
    [188, 188, 188, 79, 79, 79, 79, 34, 34, 34, 36, 172, 172, 143, 142, 157, 79, 79, 79, 79, 79, 79, 187, 159, 189, 79, 79, 79, 55, 55, 55, 55],
    [79, 79, 79, 79, 79, 79, 79, 79, 34, 34, 36, 172, 159, 158, 172, 143, 157, 79, 79, 79, 79, 79, 79, 79, 79, 79, 39, 51, 51, 51, 55, 55],
    [79, 79, 79, 79, 79, 79, 79, 79, 79, 34, 36, 172, 143, 142, 172, 172, 143, 157, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 55],
    [79, 79, 79, 79, 79, 79, 79, 79, 79, 34, 52, 172, 172, 172, 172, 172, 172, 143, 156, 157, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79],
    [79, 79, 79, 79, 79, 79, 79, 79, 79, 34, 52, 172, 172, 172, 172, 172, 172, 159, 188, 189, 79, 79, 79, 79, 79, 171, 172, 172, 173, 79, 79, 79],
    [79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 188, 158, 172, 172, 172, 172, 173, 79, 79, 79, 79, 79, 79, 79, 187, 158, 159, 189, 79, 79, 79],
    [79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 171, 172, 172, 159, 188, 189, 79, 79, 79, 79, 79, 79, 79, 79, 171, 173, 79, 79, 79, 79],
    [79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 171, 172, 172, 173, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 171, 173, 79, 79, 79, 79],
    [155, 142, 157, 79, 79, 79, 79, 79, 79, 79, 79, 79, 187, 188, 188, 189, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 171, 173, 79, 79, 79, 79],
    [171, 172, 173, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 171, 173, 79, 79, 79, 79],
    [171, 172, 143, 156, 157, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 187, 189, 79, 79, 79, 79],
    [187, 188, 158, 172, 173, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79],
    [79, 79, 79, 188, 189, 79, 79, 79, 79, 79, 79, 155, 156, 156, 157, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 155, 156],
    [34, 34, 79, 79, 79, 79, 79, 79, 79, 79, 79, 171, 172, 172, 173, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 155, 142, 172],
    [34, 34, 34, 79, 79, 79, 79, 79, 79, 79, 79, 171, 172, 172, 173, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 171, 172, 172],
    [34, 34, 34, 34, 79, 79, 79, 79, 79, 79, 155, 172, 172, 159, 189, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 171, 172, 172],
    [34, 34, 34, 34, 34, 34, 79, 79, 79, 79, 171, 172, 172, 173, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 155, 142, 172, 172]
  ];
var layer01 = [
    [0, 0, 32, 33, 0, 236, 0, 0, 236, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 0, 32, 33],
    [0, 0, 48, 49, 0, 236, 220, 220, 236, 0, 0, 147, 72, 73, 70, 71, 72, 73, 83, 83, 84, 85, 0, 0, 0, 0, 0, 48, 49],
    [0, 0, 64, 65, 54, 0, 236, 236, 0, 0, 162, 163, 84, 89, 86, 87, 88, 89, 99, 99, 100, 101, 0, 0, 0, 0, 7, 112, 113],
    [0, 0, 80, 81, 70, 54, 55, 50, 0, 0, 0, 179, 100, 105, 102, 103, 104, 105, 0, 0, 0, 0, 0, 0, 16, 22, 23, 39],
    [0, 0, 96, 97, 86, 70, 65, 144, 193, 0, 0, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 48, 49],
    [0, 0, 0, 0, 102, 86, 81, 160, 161, 0, 0, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64, 65, 174, 175, 67, 66, 54],
    [0, 0, 0, 0, 0, 102, 97, 176, 177, 0, 0, 37, 0, 252, 0, 0, 0, 201, 202, 0, 0, 0, 0, 0, 80, 81, 190, 191, 83, 82, 70, 71],
    [0, 0, 0, 0, 0, 0, 0, 48, 49, 0, 0, 53, 0, 0, 0, 0, 0, 217, 218, 0, 0, 0, 0, 0, 96, 97, 222, 223, 99, 98, 86, 87],
    [201, 202, 0, 0, 0, 0, 0, 64, 65, 66, 68, 69, 0, 0, 0, 0, 0, 233, 234, 0, 0, 0, 0, 0, 238, 239, 0, 0, 238, 239, 102, 103],
    [217, 218, 0, 0, 0, 0, 0, 80, 81, 82, 84, 85, 0, 0, 0, 0, 0, 249, 250, 0, 0, 0, 0, 0, 254, 255, 0, 0, 254, 255],
    [233, 234, 0, 0, 0, 0, 0, 96, 97, 98, 100, 101, 0, 0, 0, 0, 0, 0, 0],
    [249, 250, 0, 0, 201, 202, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 238, 239, 0, 0, 238, 239],
    [0, 0, 0, 0, 217, 218, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 254, 255, 0, 0, 254, 255],
    [0, 0, 0, 0, 233, 234, 196, 197, 198],
    [2, 3, 4, 0, 249, 250, 228, 229, 230],
    [18, 19, 20, 8, 0, 0, 244, 245, 246, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 201, 202],
    [0, 35, 40, 24, 25, 8, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 217, 218],
    [0, 0, 0, 40, 41, 20, 8, 9, 0, 0, 0, 0, 0, 0, 0, 16, 17, 18, 19, 20, 21, 0, 0, 0, 0, 0, 0, 0, 233, 234],
    [0, 0, 0, 0, 40, 19, 24, 25, 8, 9, 0, 0, 0, 0, 0, 48, 49, 50, 51, 52, 115, 3, 4, 0, 0, 0, 0, 0, 249, 250],
    [0, 0, 0, 0, 0, 0, 40, 41, 20, 21, 0, 0, 0, 0, 0, 64, 65, 66, 67, 52, 19, 19, 20, 21]
  ];
  var layer02 = [
    [0, 0, 0, 0, 0, 220, 0, 0, 220],
    [],
    [],
    [],
    [],
    [0, 0, 0, 0, 0, 0, 0, 0, 201, 202],
    [0, 0, 0, 0, 0, 0, 0, 0, 217, 218],
    [0, 0, 0, 0, 0, 0, 0, 0, 233, 234],
    [0, 0, 0, 0, 0, 0, 0, 0, 249, 250],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 196, 197, 198],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 228, 229, 230],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 244, 245, 246],
    []
  ];
  
//Define variables
var layers = [];
layers[0] = ground;
//layers[1] = layer01;
layers[1] = layer02;
var tilesetImage = new Image(); //the tile image
tilesetImage.src = 'src/tileset.png';
var myMap = new Map(layers, tileSize, rowTileCount, colTileCount, imageNumTiles, tilesetImage, canvas); //the map object
tilesetImage.onload = function(){myMap.render(ctx);};

//add a click event to an element to get the click position
function addClickHandler(element) {
	element.addEventListener('click', function(event) {
		select = (function() {
			return getCoords(event, element);
		})();
		console.log(select);
    }, false);
}

//Return the coordinates relative to the element and the tile number
//To do: move the tile number to the tile class
function getCoords(event, element){
	var bounding = element.getBoundingClientRect();
	var left = bounding.left;
	var top = bounding.top;
	x = event.pageX - left - document.body.scrollLeft;
	y = event.pageY - top - document.body.scrollTop;
	console.log(element.id + ' - x: ' + x + ', y: ' + y);
	console.log(t = new Tile(x, y));
	console.log(t.getTileNumber);
	return t.getTileNumber;
}

//The tile class
//To do: implement the map as an array of tile objects
var Tile = function Tile(x, y){
	this.x = x;
	this.y = y;
	this.getTileNumber = Math.floor(this.x / 32) + Math.floor(this.y / 32) * 16;
	this.row = Math.floor(this.x / 32);
	this.col = Math.floor(this.y / 32);
};

/*
Tile.prototype.getTileNumber = function(){
	return 
};*/

var select = 0;
console.log('select: ' + select);
myMap.addClickHandler();
myMap.addMouseDownHandler(select);
myMap.addMouseUpHandler(select);
myMap.addRightClickHandler();
addClickHandler(tileset);
//addClickHandler(canvas);
var j = JSON.stringify(myMap);
mappy = JSON.parse(j);
console.log(mappy);
