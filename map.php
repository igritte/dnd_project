<?php
// Old code: not being used
require_once 'header.php';
include 'helper.php';
get_header();
?>

        
<style>
canvas {
	display: block;
}
.selected {
	font-style: italic;
	color: #FFFFFF;
	background: #a90329; /* Old browsers */
	/* IE9 SVG, needs conditional override of 'filter' to 'none' */
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2E5MDMyOSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjQ0JSIgc3RvcC1jb2xvcj0iIzhmMDIyMiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM2ZDAwMTkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
	background: -moz-linear-gradient(left,  #a90329 0%, #8f0222 44%, #6d0019 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, right top, color-stop(0%,#a90329), color-stop(44%,#8f0222), color-stop(100%,#6d0019)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(left,  #a90329 0%,#8f0222 44%,#6d0019 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(left,  #a90329 0%,#8f0222 44%,#6d0019 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(left,  #a90329 0%,#8f0222 44%,#6d0019 100%); /* IE10+ */
	background: linear-gradient(to right,  #a90329 0%,#8f0222 44%,#6d0019 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a90329', endColorstr='#6d0019',GradientType=1 ); /* IE6-8 */
}
#editor {
	width: 512px;
	height: 640px;
	border: 1px solid;
	position: absolute;
	right: 0;
	top: 0;
}
.page {
	position: relative;
}

.layer {
	padding-left: 5px;
	border-bottom: 1px solid #890000;
}
#layers {
	height: 200px;
	width: 512px;
	font-size: 24px;
	position: relative;
	float: left;
}
#layers button {
	position: absolute;
	bottom: 10px;
	left: 5px;
}
#selector {
	width: 32px;
	height: 32px;
	border: 1px solid yellow;
	display: none;
	position: absolute;
	z-index: 1;
}
#canvas {
	border: 5px solid white;
	width: 512px;
	height: 320px;
}
#selector2 {
	width: 32px;
	height: 32px;
	border: 1px solid yellow;
	display: none;
	position: absolute;
	z-index: 1;
}
#save_button {
	width: 200px;
	height: 50px;
	background: url('src/button.png');
	cursor: pointer;
}
</style>
<body>
	<div class="wrap">
		<div class="header">
			<div class="logo"></div>
			<header class="clear">
					<nav id="nav_menu">
						<ul>
							<li><a href="#" class="home" title="Home">Home</a></li>
							<li><a href="map.php" class="media" title="Media">Media</a></li>
							<li><a href="#" class="tavern" title="Tavern">Tavern</a></li>
							<li><a href="#" class="game_info" title="Game info">Game info</a></li>
							<li><a href="#" class="faq"  title="FAQ">FAQ</a></li>
						</ul>
					</nav>
					<div id="profile">
						<div class="char_info">
						<?php if (!is_logged_in()) {
							echo "You're not logged in. Login or Register to start playing.";
						} else if (!selected_char()) {
							echo "You haven't selected a character yet. Create one here.";
						} else {
							echo "You're playing as " . $_SESSION['char'];
						}
						?></div>
						<div id="avatar"><img src="src/tokens/male_dwarf.png"></div>
						<div id="profile_options">
							<ul id="options">
								<li>Manage profile</li>
								<li>Create a new Character</li>
								<li>Create a party</li>
							</ul>
						</div>
					</div>
			</header>
		</div>
	</div>
		<div id="selector"></div>
		<div id="selector2"></div>
	<div id="main">
		<div class="page">
				<div id="canvas">
					<canvas id="map" width="512" height="320"></canvas>
				</div>
			<div id="editor">
				<canvas id="minimap" width="512" height="320"></canvas>
					<div id="layers">
						<button id="add" value="add">Add</button>
					</div>
					<form method="post" id="upload_file">
					<input type="file" id="file" name="file"/><span id="upload_error"></span><input type="submit" id="upload" value="upload"/>
					</form>
				<img id="tileset" alt="" src="src/tile.png">
				<div id="save_button"></div>
			</div>
		</div>
		<canvas id="mmap" width="1024" height="640"></canvas>
	</div>
</body>
<script src="js/test.js"></script>
<script>
var layer = 0;
var selected_layer = '';
$( document ).ready(function() {
    $('#avatar').bind("contextmenu", function(event) {
    	event.preventDefault();
		$("#profile_options").fadeIn(500, startFocusOut());
    });
    var map = document.getElementById('map');
    var ctx = map.getContext('2d');
    //cloneCanvas(map);
    //minimap.drawImage(map, 0, 0);
});
	
function cloneCanvas(oldCanvas) {

    //create a new canvas
    var newCanvas = document.getElementById('minimap');
    var context = newCanvas.getContext('2d');

    //set dimensions
    newCanvas.width = oldCanvas.width * 0.4;
    newCanvas.height = oldCanvas.height * 0.4;

    //apply the old canvas to the new one
    context.drawImage(oldCanvas, 0, 0);

    //return the new canvas
    return newCanvas;
}
function startFocusOut() {
    $(document).on("click", function () {   
        $("#profile_options").hide(500);              // To hide the context menu
        $(document).off("click");           
    });
}
</script>
</html>