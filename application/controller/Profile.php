<?php
class Profile extends BaseController
{
	function __construct()
	{
		Auth::isUserOnline();
		parent::__construct();
		$this->view->header = "header.php";

	}

	public function index()
	{
		$this->view->content = $this->model->getCharacters($_SESSION['user_id']);
		$this->view->render('profile/index');
	}
	
	public function edit()
	{
		if (isset($_POST['password'])) {
			$username = $_SESSION['username'];
			$password = sha1($_POST['password']);
		}
		
		try {
			if($this->model->edit($username, $password)) {
				echo json_encode( array( "status" => "success", "msg" => "Your password has been changed successfully"));
			} else {
				echo json_encode( array( "status" => "fail", "msg" => "An error occurred."));
			}
		} catch (Exception $e) {
			echo json_encode( array( "status" => "fail", "msg" => "Cannot change password."));
		}
	}
	
	public function selectCharacter()
	{
		if (isset($_POST['char'])) {
			$_SESSION['character'] = $_POST['char'];
			$this->model->saveChar($_POST['char']);
		}
	}
}