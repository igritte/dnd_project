<?php
class Login extends BaseController
{	
	function __construct()
	{
		parent::__construct();
		$this->view->header = "header_front.php";
		$this->view->styles = "styles/styles-front.css";
		$this->view->menu = "menu_front.php";
	}
	
	public function index()
	{
		$this->view->render('login/index');
	}
	
	public function login()
	{
		if (isset($_POST['username']) && isset($_POST['password'])) {
			$username=$_POST['username'];
			$password = sha1($_POST['password']);
		}
		
		try {
			if($this->model->login($username, $password)) {
				$_SESSION["username"] = $username;
				if (isset($_POST['remember']) && $_POST['remember'] == 1) {
					setcookie("username", $username);
				}
				echo json_encode( array( "status" => "success", "msg" => "successful"));
			} else {
				echo json_encode( array( "status" => "fail", "msg" => "Invalid username or password."));
			}
		} catch (Exception $e) {
			echo json_encode( array( "status" => "fail", "msg" => "Invalid username or password."));
		}
	}
	
	public function logout()
	{
		setcookie('username', false, time() - (3600 * 3650));
		
		@session_destroy();
		
		header('location: ' . URL . 'login');
	}
	
	public function register()
	{
		if (isset($_POST['username']) && isset($_POST['email']) && isset($_POST['password'])) {
			$username=$_POST['username'];
			$password = sha1($_POST['password']);
			$email = $_POST['email'];
		}
		
		try {
			if($this->model->login($username, $password)) {
				echo json_encode( array( "status" => "fail", "msg" => "Username already exists."));
			} else {
				$this->model->register($username, $email, $password);
				echo json_encode( array( "status" => "success", "msg" => "Registration complete."));
			}
		} catch (Exception $e) {
			echo json_encode( array( "status" => "fail", "msg" => "Registration failed."));
		}
	}
}
