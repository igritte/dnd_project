<?php
/*
 * To do: error handling and custom error page
 */
class Error extends BaseController
{
	function __construct()
	{
		Auth::isUserOnline();
		parent::__construct();
	}
	
	public function index()
	{
		$this->view->render('error/index');
	}
}