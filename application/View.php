<?php
class View {
	public $scripts;
	public $styles;
	public $header;
	public $footer;
	public $menu;
	
	function __construct()
	{
		$this->header = "header.php";
		$this->header = "footer.php";
	}
	
	public function render($name)
	{
		require "application/view/" . $this->header;
		require "application/view/" . $this->menu;
		
		require_once 'view/' . $name . '.php';
		
		require "application/view/"  . $this->footer;
	}
}