/**
 * 
 */

define(["js/lib/class"], function() {
	
	var Scene = Class.extend({
		init: function(obj, scene, scale) {
			this.scene = scene;
			this.rows = obj.rows || 20;
			this.cols = obj.cols || 32;
			this.layers = obj.layers;
			this.tilesize = obj.tilesize || 32;
			this.imageNumTiles = obj.imageNumTiles || 16;
			if (typeof tilesetImage === 'undefined') {
				this.tilesetImage = new Image();
				this.tilesetImage.src = 'src/characters.png';
			} else {
				this.tilesetImage = obj.tilesetImage;
			}
			this.scale = scale || 1;
		},
		
		render: function() {
			var ctx = this.scene.getContext('2d');
			var tilesize = this.tilesize;
			if (this.scale != 1)
				ctx.scale(this.scale, this.scale);
			for (var r = 0; r < this.rows; r++) {
				for (var c = 0; c < this.cols; c++) {	
					for (var index = 0; index < this.layers.length; index++) {
						var tile = this.layers[index][ r ][ c ],
							tileRow = (tile / this.imageNumTiles) | 0,
							tileCol = (tile % this.imageNumTiles) | 0;
						ctx.drawImage(this.tilesetImage, (tileCol * tilesize),
									(tileRow * tilesize), tilesize, tilesize,
									(c * tilesize), (r * tilesize), tilesize, tilesize);
					}
				}
			}
		},
		
		renderTile: function(row, col)	{
			var tilesize = this.tilesize;
			var ctx = this.scene.getContext('2d');
			ctx.clearRect(col * tilesize, row * tilesize, tilesize, tilesize);
			for (var index = 0; index < this.layers.length; index++) {
				var tile = this.layers[index][row][col];
				var tileRow = (tile / this.imageNumTiles) | 0;
				var tileCol = (tile % this.imageNumTiles) | 0;
				ctx.drawImage(this.tilesetImage, (tileCol * this.tilesize),
							(tileRow * this.tilesize), this.tilesize, this.tilesize,
							(col * this.tilesize), (row * this.tilesize), this.tilesize, this.tilesize);
			}
		},
		
		getTile: function(x, y) {
			var row = (y / this.tilesize) | 0;
			var col = (x / this.tilesize) | 0;
			return {
		        row: row,
		        col: col
		    };  
		},
		
		setTile: function(row, col, layer, selected) {
			this.layers[layer][row][col] = selected;
		},
		
		clearTile: function(row, col) {
			console.log('1: ' + this.layers[0][row][col]);
			console.log(row, col);
			this.layers[0][row][col] = 0;
			console.log('2: ' + this.layers[0][row][col]);
			this.scene.getContext('2d').clearRect(col * this.tilesize, row * this.tilesize,
					this.tilesize, this.tilesize);
		},
		
		addLayer: function(layer) {
			if (typeof layer === 'undefined') {
				var layer = [];
				for (var index = 0; index < this.cols; index++) {
					layer.push([]);
				}
			}
			this.layers.push(layer);
		},
		
		getLayersNum: function() {
			return this.layers.length;
		},
		
		getScale: function() {
			return this.scale;
		},
		
		getHeight: function() {
			return this.rows * this.tilesize;
		},
		
		getWidth: function() {
			return this.cols * this.tilesize;
		},

		getTilesize: function() {
			return this.tilesize;
		}
	});
	
return Scene;
});