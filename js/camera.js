/*
 * Camera object, handles basic world to grid coordinates conversion and boundary checks
 */
define(["js/lib/class"], function() {
	
	var Camera = Class.extend({
		init: function(x, y, mapWidth, mapHeight, worldWidth, worldHeight) {
			this.x = x || 0;
			this.y = y || 0;
			
			this.gridH = mapHeight;
			this.gridW = mapWidth;
			
			this.height = worldHeight;
			this.width = worldWidth;
			
			this.followed = null;
		},
		
		getPosition: function() {
			return {'x': this.x,
					'y': this.y};
		},
		
		setPosition: function(x, y, scale) {
			this.x = x / scale;
			this.y = y / scale;
		},
		
		checkCoords: function(x, y) {
			var left = x,
				top = y,
				bottom = y + this.gridH,
				right = x + this.gridW,
				newX, newY;
			if (!this.isInBounds(left, 0))
				newX = 0;
			else if (!this.isInBounds(right, 0))
				newX = this.width - this.gridW;
			else
				newX = left;
			if (!this.isInBounds(0, top))
				newY = 0;
			else if (!this.isInBounds(0, bottom))
				newY = this.height - this.gridH;
			else
				newY = top;
			return {'x': newX,
					'y': newY};
		},

		normalizeCoords: function(x, y, scale) {
			var left = x /scale - this.gridW / 2,
				top = y / scale - this.gridH / 2;
			return {'x': left, 'y' : top};
		},
		
		isVisible: function(x, y, scale) {
			x = x / scale;
			y = y / scale;
			if (this.x <= x && this.y <= y && this.x + this.gridW > x && this.y + this.gridH > y ) {
				return true;
			}
			return false;
		},
		
		isInBounds: function(x, y) {
			console.log('bounds' + x + ' ' + y + ' ' + this.width + ' ' + this.height);
			if (x > this.width ||
				y > this.height	||
				x < 0 ||
				y < 0) {
				return false;
			}
			else
				return true;
		},
		
		isGridInBounds: function(x, y, scale) {
			return this.isInBounds(x + this.gridW * scale, y + this.gridH * scale, scale);
		},
		
		follow: function(entity) {
			this.followed = entity;
		},
		
		lookAt: function() {
			//todo: make the camera look at objects
		}
	});
	
	return Camera;
});