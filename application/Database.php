<?php
/*
 * Basic wrapper for PDO
 * Source: http://culttt.com/2012/10/01/roll-your-own-pdo-php-class/
 */
class Database
{
	private $db_handler;
	private $error;
	private $stmt;
	
	public function __construct(PDO $pdo)
	{
		$this->db_handler = $pdo;
	}
	
	public function prepare($query)
	{
		$this->stmt = $this->db_handler->prepare($query);
	}
	
	public function execute()
	{
		return $this->stmt->execute();
	}
	
	public function getResult()
	{
		$this->execute();
		return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function rowCount()
	{
		return $this->stmt->rowCount();
	}
	
	public function bind($pos, $value, $type = null) {
	
		if( is_null($type) ) {
			switch( true ) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
			}
		}
	
		$this->stmt->bindValue($pos, $value, $type);
		return $this;
	}
	
	public function delete($table, $where, $what)
	{
		$query = "DELETE FROM " . $table . " WHERE " . $where . "=" . $what . ";";
		$this->prepare($query);
		$this->execute();
	}
	
	public function select($table, $columns="*", $where="", $what="")
	{
		$query = "SELECT " . $columns . " FROM " . $table;
		if (!empty($where) && !empty($what)) {
			$query .= " WHERE " . $where . "=" . "'$what'";
		}
		$this->prepare($query);
		$this->execute();
	}
	
	public function beginTransaction(){
		return $this->db_handler->beginTransaction();
	}
	
	public function endTransaction(){
		return $this->db_handler->commit();
	}
	
	public function cancelTransaction(){
		return $this->db_handler->rollBack();
	}
	
	public function setAttribute($attribute, $value=true)
	{
		$this->db_handler->setAttribute($attribute, $value);
	}
}