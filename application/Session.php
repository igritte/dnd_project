<?php
class Session
{
	public $properties;
	
	public function __construct()
	{
		$this->init();
	}
	
	public function init()
	{
		@session_start();
		$this->properties = $_SESSION;
	}
	
	public function __get($key)
	{
		if (array_key_exists($key, $this->properties)) {
			return $this->properties[$key];
		}
	
		return null;
	}
	
	public function __set($key, $value)
	{
		$this->properties[$key] = $value;
	}
	
	public function exists($key)
	{
		return isset($this->properties[$key]);
	}
	
	public function delete($key)
	{
		unset($_SESSION[$key]);
	}
}