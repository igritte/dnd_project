
define(["js/map", "js/camera", "js/character", "js/mob", "js/scene"], function(Map, Camera, Character, Mob, Scene) {
	var Game = Class.extend({
		init: function(player, map, scene) {
			this.map = map;
			this.scene = scene;
			this.player = player;
			this.mobs = [];
			this.camera = new Camera(0, 0, 544, 352, this.map.getWidth(), this.map.getHeight());
			this.log = null;
			
			this.combat = false;
			this.turnOrder = [];
			this.turnOwner = null;
			this.movesLeft = 0;
			this.actionsLeft = 0;
			this.win = false;
		},
		
		addMob: function(mob) {
			var mobTile = this.scene.getTile(mob.x, mob.y);
			this.scene.setTile(mobTile.row, mobTile.col, 0, 9);
			this.mobs.push(mob);
		},
		
		start: function() {
			var tile = this.scene.getTile(this.player.x, this.player.y);
			this.scene.setTile(tile.row, tile.col, 0, this.player.token);
			this.camera.follow(this.player);
		},
		
		movePlayer: function(x, y) {
			if (game.camera.isInBounds(x + 1, y + 1) && this.checkForCollision(x, y)) {
		    	var tile = this.scene.getTile(x, y);
		    	var tilePlayer = this.scene.getTile(this.player.x, this.player.y);
		    	//this.scene.clearTile(tilePlayer.row, tilePlayer.col);
		    	this.scene.scene.getContext('2d').clearRect(0, 0, this.scene.cols*this.scene.tilesize, this.scene.rows*this.scene.tilesize );
		    	this.scene.setTile(tilePlayer.row, tilePlayer.col, 0, 0);
		    	this.scene.setTile(tile.row, tile.col, 0, 1);
		    	game.player.setPosition(x, y);
				this.scene.render();
				if (this.movesLef != 0 && game.combat == true)
					this.movesLeft -= 1;
		    	return true;
			}
			return false;
		},
		
		normalizeCoords: function(x, y)	{
			var tilesize = this.map.getTilesize();
			var normalized = this.camera.normalizeCoords(x + tilesize/2, y + tilesize/2, this.map.getScale());
			return normalized;
		},
		
		cameraFollow: function(cameraX, cameraY) {
			var normalized = this.normalizeCoords(this.camera.followed.x, this.camera.followed.y, 1),
				checked = this.camera.checkCoords(normalized.x, normalized.y),
				//newChecked = this.camera.checkCoords(this.player.x-transX, this.player.y-transY),
				foreground = this.player.scene.getContext('2d'),
				map = this.map.scene.getContext('2d');
			console.log('camera ' + cameraX + ' ' + cameraY);
			console.log('normalized ' + normalized.x + ' ' + normalized.y);
			console.log('checked ' + checked.x + ' ' + checked.y);
			//console.log('player ' + this.camera.followed.x + ' ' + this.camera.followed.y);
			if ((checked.x != cameraX || checked.y != cameraY)) {
				map.translate(cameraX - checked.x, cameraY - checked.y);
				this.map.render();
		    	this.scene.scene.getContext('2d').clearRect(0, 0, this.scene.cols*this.scene.tilesize, this.scene.rows*this.scene.tilesize );
				foreground.translate(cameraX - checked.x, cameraY - checked.y);
				this.scene.render();
		    	this.camera.setPosition(checked.x, checked.y, 1);
		    	
			}
		},
		
		visibleEnemies: function() {
			for (var i=0; i < this.mobs.length; i++) {
				if (this.camera.isVisible(this.mobs[i].x, this.mobs[i].y, this.map.getScale())) {
					this.combat = true;
					this.log.append('<p>Entering combat</p>');
					this.camera.follow(this.mobs[i]);
					var pos = this.camera.getPosition();
					this.cameraFollow(pos.x, pos.y);
					this.enterCombat();
					return;
				}
			}
		},
		
		checkForCollision: function(x, y)
		{
			var tile = this.map.getTile(x, y);
			console.log(tile);
			console.log(this.map.layers[0][tile.row][tile.col]);
			
			if (this.map.layers[0][tile.row][tile.col] == 0 || this.map.layers[0][tile.row][tile.col] == 'undefined' || this.scene.layers[0][tile.row][tile.col] !=0) {
				return false;
			}
			return true;
		},
		
		checkAlive: function()
		{
			var target = this.turnOwner.target;
			if (target.alive != true) {
				this.turnOrder.splice(this.turnOrder.indexOf(target), 1);
				var tile = this.scene.getTile(target.x, target.y);
				console.log(this.scene.layers[0][tile.row][tile.col]);
				this.scene.setTile(tile.row, tile.col, 0, 0);
				console.log(this.scene.layers[0][tile.row][tile.col]);
				this.turnOwner.target = null;
				this.mobs.splice(this.mobs.indexOf(target), 1);
				this.log.append('<p>'+ target.name + ' died.</p>');
			    this.scene.scene.getContext('2d').clearRect(0, 0, this.scene.cols*this.scene.tilesize, this.scene.rows*this.scene.tilesize );
				this.scene.render();
				this.checkCombat();
			}
		},
		
		checkCombat: function() {
			if (this.turnOrder.indexOf(this.player) == -1) {
				this.defeat();
			} else if (this.turnOrder.length == 1){
				this.leaveCombat();
			}
		},
		
		enterCombat: function()	{
			this.turnOrder = this.rollInitiative();
			this.switchTurn();
		},
		
		leaveCombat: function() {
			this.log.append('<p>Leaving combat.</p>');
			this.checkWin();
			this.camera.follow(this.player);
			this.combat = false;
			this.player.HP = this.player.maxHP;
			$('#hp').width(100);
		},
		
		checkWin: function() {
			if (this.mobs.length == 0) {
				this.win = true;
				this.log.append('<p>You won.</p>');
				$('#end').html('YOU WIN!');
			}
		},
		
		defeat: function() {
			this.log.append('<p>You have been defeated.</p>');
			$('#end').html('You have been defeated!');
		},
		
		rollInitiative: function() {
			var actors = [];
			actors.push(this.player);
			for (var i=0; i < this.mobs.length; i++) {
				if (this.camera.isVisible(this.mobs[i].x, this.mobs[i].y, this.map.getScale())) {
					actors.push(this.mobs[i]);
				}
			}
			actors = this.shuffleArray(actors);
			console.log(actors);
			return actors;
		},
		
		shuffleArray: function (array) {
		    for (var i = array.length - 1; i > 0; i--) {
		        var j = Math.floor(Math.random() * (i + 1));
		        var temp = array[i];
		        array[i] = array[j];
		        array[j] = temp;
		    }
		    return array;
		},
		
		switchTurn: function() {
			this.turnOwner = this.turnOrder.shift();
			this.turnOwner.target = null;
			this.log.append("<p>It's " + this.turnOwner.name + "'s turn.</p>");
			this.turnOrder.push(this.turnOwner);
			this.actionsLeft = 2;
			this.log.append('<p>Actions left this turn ' + this.actionsLeft + '</p>');
			console.log(this.turnOwner, this.player);
			if (this.turnOwner !== this.player) {
				console.log(this.turnOwner);
				this.mobAI();
			}
		},
		
		switchTarget: function() {
			if (this.turnOwner.target == null) {
				this.turnOwner.target = this.turnOrder[0];
			} else {
				this.turnOwner.target = this.turnOrder[ (this.turnOrder.indexOf(this.turnOwner.target) + 1) % this.turnOrder.length];
			}
			this.log.append('<p>' + this.turnOwner.name + ' has targetted ' + this.turnOwner.target.name + '</p>');
		},
		
		mobAI: function() {
			this.turnOwner.target = this.player;
			while(this.actionsLeft > 0) {
				var roll = Math.floor((Math.random() * this.turnOwner.maxHP) + 1);
				if (this.turnOwner.HP < roll) {
					this.turnOwner.heal();
					this.actionsLeft -= 1;
				} else {
					this.turnOwner.attack();
					this.actionsLeft -= 1;
					this.checkAlive();
				}
			}
			this.switchTurn();
		}
	});
	
	return Game;
});