<style>
#canvas {
	width: 544px;
	height: 352px;
	position: absolute;
	left: 450px;
	padding: 50px;
}
</style>

<?php 
/*
 * Separate game and turn logic
 * Implement array of characters
 * Move mob AI into the Mob class or make a separate one
 * Improve the hud - use sprites
 * Move mouse and keyboard input logic to its ows class
 * Validate user moves
 * Save progress in the database
 * Implement animation and pathfinding
 * Implement party system
 * Implement skill and stat system
 */ 
?>
<div id="main">
<div class="game">
<p class="help"> Use the arrow keys to move.</p>
<p class="help"> Use 4 to switch target and 1 to perform moving action while in combat.</p>
<p class="help"> Use 2 and 3 to use your abilities.</p>
<div id="canvas">
	<p id="end"></p>
	<canvas id="map" width="544" height="352"></canvas>
	<canvas id="foreground" width="544" height="352"></canvas>
</div>
<div class="hud">
<?php if (isset($_SESSION['gender']) && $_SESSION['gender'] == 'm') { ?>
	<img alt="" src="src/tokens/male_<?php echo $_SESSION['char_race']; ?>.png">
<?php } else { ?>
	<img alt="" src="src/tokens/female_<?php echo $_SESSION['char_race']; ?>.png">
<?php }?>
<p id="char_name"><?php echo $_SESSION['character']; ?></p>

<div id="hp_container"><div id="hp"></div></div>
</div>
<div id="ui">
	<div class="abilities" id="move">Move</div>
	<div class="abilities" id="melee">Attack</div>
	<div class="abilities" id="heal">Heal</div>
</div>
	<div id="log"></div>
	<button id="pass_turn">Pass Turn</button>
</div>
</div>

<script>
$(document).ready(function() {
require(["js/map", "js/camera","js/game", "js/character", "js/mob", "js/scene"], function(Map, Camera, Game, Character, Mob, Scene) {
	var tokens = {'dragonbornm' : 1, 'dwarfm' : 2, 'eladrinm' : 3, 'elfm' : 4,
	        	'half-elfm' : 5, 'halflingm' : 6, 'humanm' : 7, 'tieflingm' : 8,
	        	'dragonbornf' : 17, 'dwarff' : 18, 'eladrinf' : 19, 'elff' : 20,
	        	'half-elff' : 21, 'halflingf' : 22, 'humanf' : 23, 'tieflingf' : 24};
	var canvas = document.getElementById('map');
	var mapData = JSON.parse(<?php echo json_encode($this->content['map_data']); ?>);
	var map = new Map(mapData, canvas, 1);
	var character = <?php echo json_encode($this->content['character'][0]); ?>;
	console.log(character);
	var mob = <?php echo json_encode($this->content['character'][0]); ?>;
	var token = character.race_name + character.gender;
	for(var key in tokens) {
		if (key == token) {
			token = tokens[key];
		}
	}
	character.token = token;
	var tilesetImage = new Image(); //the tile image
	tilesetImage.src = 'src/characters.png';
	var array = [];
	for (var i = 0; i < 1; i++) {
		array.push([]);
		for (var j = 0; j < map.rows; j++) {
			array[i].push([]);
			for (var int2 = 0; int2 < map.cols; int2++) {
				array[i][j].push(0);
			}
		}
	}
	var mapJSON = {	'rows' : map.rows,
					'cols' : map.cols,
					'layers' : array,
					'tilesize' : map.tilesize,
					'imageNumTiles' : map.imageNumTiles,
					'tilesetImage' : tilesetImage,
					'scale' : 1};
	//var mobJSON = {
	var charCanvas = document.getElementById('foreground');
	var scene = new Scene(mapJSON, charCanvas, 1);
	var player = new Character(character, tilesetImage, charCanvas);
	game = new Game(player, map, scene);
	var player2 = new Mob(mob, tilesetImage, charCanvas);
	var player3 = new Mob(mob, tilesetImage, charCanvas);
	var player4 = new Mob(mob, tilesetImage, charCanvas);
	var mobs = [];
	for (var i=0; i <= 3; i++) {
		var charSet = false;
		var mob = new Mob(mob, tilesetImage, charCanvas);
		mobs.push(mob);
		while (charSet == false) {
			var row = Math.floor((Math.random() * map.rows) + 0);
			var col = Math.floor((Math.random() * map.cols) + 0);
			console.log('row ' + row + 'col ' + col);
			if (map.layers[0][row][col] != 0 && map.layers[0][row][col] != 'undefined' && scene.layers[0][row][col] ==0) {
				charSet = true;
				mobs[i].x = col * map.tilesize;
				mobs[i].y = row * map.tilesize;
				mobs[i].name = 'gad' + i;
				game.addMob(mobs[i]);
			}
			console.log(game.mobs);
		}
	}
	tilesetImage.onload = function(){
		game.log = $('#log');
		game.start();
		game.map.render();
		game.scene.render();	
	};
	//console.log(game.map.cols * game.map.tilesize, game.map.rows * game.map.tilesize);
	//console.log(game.scene.cols * game.scene.tilesize, game.scene.rows * game.scene.tilesize);
	$('#minimap').on('click', function(event) {
		ctx = $('#map')[0].getContext('2d'),
		pos = editor.camera.getPosition(),
		scale = editor.minimap.getScale(),
		normalized = editor.camera.normalizeCoords(x, y, scale),
		realCoords = editor.camera.checkCoords(normalized.x, normalized.y);
		
		//console.log('pos' + pos.x + ' ' + pos.y);
		ctx.translate(pos.x - realCoords.x, pos.y - realCoords.y);
		editor.camera.setPosition(realCoords.x, realCoords.y, 1);
		//console.log('bounds' + editor.camera.isGridInBounds(x, y, scale));
    	editor.map.render();
	});
	
	$(document).on('keyup', function(e) {
		$("#log").scrollTop($("#log")[0].scrollHeight);
		e.stopPropagation();
		e.preventDefault();
		if (game.win == true) {
			
		} else if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
		    var	tilesize = game.map.getTilesize(),
				pos = game.camera.getPosition(),
				transX,transY;
		    if (e.keyCode == 40) {
		    	// down arrow
		    	transX = 0;
		    	transY = -tilesize;
		    } else if (e.keyCode == 39) {
		    	// right arrow
		    	transX = -tilesize;
		    	transY = 0;
		    } else if (e.keyCode == 37) {
		    	// left arrow
		    	transX = tilesize;
		    	transY = 0;
		    } else if (e.keyCode == 38) {
		    	// up arrow
		    	transX = 0;
		    	transY = tilesize;
		    }
			console.log('vhod' + (game.player.x - transX)/32 + ' ' + (game.player.y - transY)/32);

			if (game.combat == false) {
				game.movePlayer(game.player.x - transX, game.player.y - transY);
			    game.cameraFollow(pos.x, pos.y);
			    game.visibleEnemies();
			} else if (game.movesLeft > 0 && game.turnOwner == game.player) {
				game.movePlayer(game.player.x - transX, game.player.y - transY);
			}
	    } else if ((e.keyCode == 49 || e.keyCode == 50  || e.keyCode == 51
	    	    || e.keyCode == 52) && game.combat == true
	    	     && game.turnOwner == game.player && game.actionsLeft > 0) {
			if (e.keyCode == 49) {
				game.movesLeft += game.player.speed;
				game.actionsLeft -= 1;
			} else if (e.keyCode == 52) {
				game.switchTarget();
			} else if (e.keyCode == 50) {
				game.player.attack(120);
				game.checkAlive();
				game.actionsLeft -= 1;
			} else if (e.keyCode == 51) {
				game.player.heal(20);
				game.actionsLeft -= 1;
			}
			game.log.append('<p>Actions left this turn ' + game.actionsLeft + '</p>');
	    }
	});

	$('#pass_turn').on('click', function() {
		if (game.combat == true) {
			game.switchTurn();
			console.log(game.turnOwner);
		}
	});
});
});
</script>