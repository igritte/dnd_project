<?php
class Play extends BaseController
{
	function __construct()
	{
		Auth::isUserOnline();
		parent::__construct();
	}

	public function index()
	{
		if (isset($_SESSION['map']) && isset($_SESSION['character'])) {
			$this->view->content = $this->model->getMap($_SESSION['map']);
			$this->view->render('play/index');
		} else {
			header("Location:" . URL . "dungeon");
		}
	}
	
	public function getMap()
	{
		if (isset ($_POST['map'])) {
			$_SESSION['map'] = $_POST['map'];
		} else {
			echo json_encode( array( "status" => "fail", "msg" => "An error occurred while loading the map."));
		}
	}
}