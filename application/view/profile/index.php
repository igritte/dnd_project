<style>
.characters div {
	margin-bottom: 10px;
}
</style>
<div id="main">
	<div class="page">
	<form method="post" id="edit_profile" action="<?php echo URL;?>profile/edit">
			<p>Change your password: </p>
			<label for="Password">Password &nbsp;</label><input type="password" id="profile_pass" name="password"/>
			<label for="Rpassword">Repeat &nbsp;</label><input type="password" id="profile_rpass" name="rpassword"/><span id="rpass_error"></span><span id="edit_status"></span>
			<input id="submit" type="submit" name="edit" value="Edit"/>
	</form>
	<p>Characters:</p>
	<span id="msg"></span>
	<div class="characters">
	<?php if (!empty($this->content)) {
	foreach ($this->content as $character) {
		if ($character['gender'] == 'f') { 
	?> 
	<div class="race_img female" id="<?php echo $character['race_name']; ?>"></div>
	<?php } else {?>
	<div class="race_img" id="<?php echo $character['race_name']; ?>"></div>
	<?php } ?>
	<div class="description">
		<p><label>Name: </label><?php echo $character['name']; ?></p>
		<p><label>Exp: </label><?php echo $character['exp']; ?></p>
		<p><label>Level: </label><?php echo $character['exp'] + 1; ?></p>
		<p><label>Race: </label><?php echo $character['race_name'];?></p>
		<p><label>Class: </label><?php echo $character['class_name'];?></p>
		<button id="<?php echo $character['id']; ?>">Choose</button>
	</div>
	<?php 	}
	} else {
		echo "<p>You don't any characters yet.</p>";
	}
	?>
	</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo URL;?>js/edit_profile.js"></script>
<script type="text/javascript">
					
	$('#avatar').bind("contextmenu", function(event) {
	event.preventDefault();
	$("#profile_options").fadeIn(500, startFocusOut());
});
	function startFocusOut() {
	    $(document).on("click", function () {   
	        $("#profile_options").hide(500);              // To hide the context menu
	        $(document).off("click");           
	    });
	}

	
</script>