<?php

/*
 * base for different validator classes
 */

abstract class Validator
{
	protected $message;
	
	public final function getMessage()
	{
		return $this->message;
	}
	
	public abstract function isValid($value);
}