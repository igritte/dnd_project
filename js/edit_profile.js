/**
 * 
 */
var initProfileApp = function() {
	
	$('#profile_rpass').on('input', function() {
		if ($(this).val() != '' && $(this).val() != $('#profile_pass').val()) {
			$('#rpass_error').addClass('error').html('Passwords do not match');
		} else {
			$('#rpass_error').removeClass().html('');
		}
	});
	
	$('#edit_profile').on("submit", function(event) {
		event.preventDefault();
		   $.ajax({
		       url: $('#edit_profile').attr("action"),
		       data: $('#edit_profile').serialize(),
		       type: "POST",
		       dataType: "json",
		       success: function(response) {
					if (response.status === 'fail') {
						$('#rpass_error').addClass('error').html(response.msg);
					} else {
						$('#edit_status').html("Success");
					}
				}
		  });
		   return false;
	});
	
	$('button').on('click', function() {
		console.log(document.URL + '/selectCharacter');
		$.ajax({
		       url: document.URL + '/selectCharacter',
		       data: {'char' : $(this).attr('id')},
		       type: "POST",
		       success: function(response) {
					if (response.status === 'fail') {
						$('#msg').addClass('error').html(response.msg);
					} else {
						$('#msg').html("Success");
						window.location.reload(false); 
					}
				}
		  });
	});
};

initProfileApp();