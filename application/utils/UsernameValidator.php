<?php
include_once 'Validator.php';

class UsernameValidator extends Validator
{
	public function isValid($value)
	{
		if (preg_match('/^[\w]{5,20}$/', $value)) {
			return true;
		} else {
			$this->message = "Username must be between 5 and 20 symbols and contain only letters";
			return false;
		}
	}
}