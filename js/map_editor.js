/*
 * 
 */
define(["js/map", "js/camera"], function(Map, Camera) {
	
	var MapEditor = Class.extend({
		init: function(obj, scene, miniScene) {
			this.map = new Map(obj, scene);
			this.minimap =new Map(obj, miniScene, 0.5);
			this.camera = new Camera(0, 0, 512, 320, this.map.getWidth(), this.map.getHeight());
		},
		
		render: function() {
			this.map.render();
			this.minimap.render();
		},
		
		render_map: function() {
			this.map.render();
		},
		
		render_minimap: function() {
			this.minimap.render();
		},
		
		renderTile: function(row, col) {
			this.map.renderTile(row, col);
			this.minimap.renderTile(row, col);
		},
		
		addLayer: function(layer) {
			this.map.addLayer(layer);
		},
		
		setCameraPosition: function(x, y) {
			this.camera.setPosition(x, y);
		},
		
		setCameraGridPosition: function(x, y) {
			this.camera.setGridPosition(x, y);
		},
		
		isVisible: function(x, y, scale) {
			return this.camera.isVisible(x, y, scale);
		},
		
		isInBounds: function(x, y, scale) {
			return this.camera.isInBounds(x, y, scale);
		},
		
		save: function() {
			return JSON.stringify(this.map);
		}
		
	});
	
	return MapEditor;
});