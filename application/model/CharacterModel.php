<?php
/*
 * Character creation
 */
class CharacterModel extends Model
{
	public $races = array();
	public $classes = array();
	public $stats = array(1 => 'strength', 2 => 'constitution',
						  3 => 'dexterity', 4 => 'intelligence',
						  5 => 'wisdom', 6 => 'charisma');
	public $skills = array();
	
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * init
	 * initializes the character creation model by retrieving all relevant character information from the database
	 * @return array 
	 */
	public function init()
	{
		$this->getClasses();
		$this->getRaces();
		$this->getSkills();
		$data['race'] = $this->races;
		$data['class'] = $this->classes;
		$data['stat'] = $this->stats;
		$data['skill'] = $this->skills;
		return $data;
	}
	
	public function getRaces()
	{
		$this->db->prepare("SELECT * FROM races WHERE playable=1");
		$this->db->execute();
		$data = $this->db->getResult();
		foreach ($data as $row => $value) {
			$this->races[$row] = $value;
		}
	}
	
	public function getClasses()
	{
		$this->db->prepare("SELECT * FROM classes WHERE playable=1");
		$this->db->execute();
		$data = $this->db->getResult();
		foreach ($data as $row => $value) {
			$this->classes[$row] = $value;
		}
	}
	
	public function getSkills()
	{
		$this->db->prepare("SELECT c.class_name, s.*
							FROM classes c
							JOIN class_to_skill cs ON c.id = cs.class_id
							JOIN skills s ON s.id = cs.skill_id
							WHERE playable =1");
		$this->db->execute();
		$data = $this->db->getResult();
		foreach ($data as $row => $array) {
			$this->skills[$array['class_name']]['skill_name'][] = $array['skill_name'];
			$this->skills[$array['class_name']]['skill_range'][] = $array['skill_range'];
			$this->skills[$array['class_name']]['lvl_requirement'][] = $array['lvl_requirement'];
			$this->skills[$array['class_name']]['num_of_targets'][] = $array['num_of_targets'];
			$this->skills[$array['class_name']]['description'][] = $array['description'];
			$this->skills[$array['class_name']]['frequency'][] = $array['frequency'];
			$this->skills[$array['class_name']]['effort'][] = $array['effort'];
			$this->skills[$array['class_name']]['area_type'][] = $array['area_type'];
		}
	}
	
	/**
	 * getClassSkills
	 * returns all skills by class from the database
	 * @param string $class
	 * @return multitype:
	 */
	public function getClassSkills($class)
	{
		foreach ($this->skills[$class] as $key => $value) {
			echo $key;
			echo $value;
		}
		return $this->skills[$class];
	}
	
	/**
	 * create
	 * saves the created character in the database
	 * @param string $name
	 * @param int $user_id
	 * @param string $race
	 * @param string $class
	 * @param int $experience
	 * @param string $gender
	 * @return boolean true if success, false otherwise
	 */
	public function create($name, $user_id, $race, $class, $experience, $gender)
	{
		try {
			$this->db->select("races", 'id', 'race_name', $race);
			$race = $this->db->getResult();
			$this->db->select("classes", 'id', 'class_name', $class);
			$class = $this->db->getResult();
			$this->db->prepare("INSERT INTO characters(name, user_id, race_id, class_id, exp, gender)
					VALUES(:name, :user_id, :race_id, :class_id, :exp, :gender)");
			$this->db->bind(":name", $name);
			$this->db->bind(":user_id", $user_id);
			$this->db->bind(":race_id", $race[0]['id']);
			$this->db->bind(":class_id", $class[0]['id']);
			$this->db->bind(":exp", $experience);
			$this->db->bind(":gender", $gender);
			
			$this->db->execute();
			return true;
		} catch (Exception $e) {
			//log this exception
			return false;
		}
	}
}