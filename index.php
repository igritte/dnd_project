<?php 
require_once 'application/utils/Autoloader.php';
require 'application/config/paths.php';
/*
$registry = new Registry();
$session = new Session();
$registry->session = $session;
*/
session_start();

$bootstrap = new Bootstrap();
$bootstrap->run();