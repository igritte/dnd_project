<?php
class Session
{
	public $started = false;
	
	private function init() 
	{
		if (session_start()) {
			$this->started = true;
			return $this->started;
		} else {
			return false;
		}
	}
	
	public static function _get($key)
	{
		if (isset($_SESSION[$key]))
			return $_SESSION[$key];
	}
	public static function __set($key, $value)
	{
		if ($this->started) {
			$_SESSION[$key] = $value;
		} else {
			
		}
	}
	
	public static function destroy()
	{
		if ($this->started) {
			unset($_SESSION);
			session_destroy();
		}
	}
}