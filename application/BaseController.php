<?php
/*
 * the base controller class that all other controllers extend
 */
abstract class BaseController {
	
	protected $model;
	protected $view;
	
	public function __construct()
	{
		$this->view = new View();
		$this->view->header = "header.php";
		$this->view->footer = "footer.php";
		$this->view->styles = "styles/styles.css";
		$this->view->menu = "menu.php";
	}
	
	/**
	 * loadModel
	 * attempts to load appropriate model when required by the controller
	 * @param string $name
	 * @param string $modelPath
	 */
	public function loadModel($name, $modelPath = 'models/') {
		$path = $modelPath . $name . "Model.php";
		
		if (file_exists($path)) {
			$name = $name ."Model";
			$this->model = new $name();
		}
	}
}