/**
 * Mouse and keybinds for the map editor
 */


require(["js/map", "js/map_editor"], function(Map, MapEditor) {
	
	var initMapEditorApp = function() {
		
	var canvas = document.getElementById("map");
	var ctx = canvas.getContext('2d');
	var tilesize = 32;       // The size of a tile (32x32)
	var rowTileCount = 20;   // The number of tiles in a row of our background
	var colTileCount = 32;   // The number of tiles in a column of our background
	var imageNumTiles = 16;  // The number of tiles per row in the tileset image
	var mini = document.getElementById('minimap');
	var mmap = document.getElementById('mmap');
	
	var tilesetImage = new Image(); //the tile image
	tilesetImage.src = 'http://localhost/kurs/DnD/src/tile3.png';
	
	//empty array as a base for our map
	var array = [];
	for (var int = 0; int < 1; int++) {
		array.push([]);
		for (var j = 0; j < 30; j++) {
			array[int].push([]);
			for (var int2 = 0; int2 < 30; int2++) {
				array[int][j].push(0);
			}
		}
	}
	
	//minimap and map JSON 
	var mapJSON = {	'rows' : rowTileCount,
					'cols' : colTileCount,
					'layers' : array,
					'tilesize' : tilesize,
					'imageNumTiles' : imageNumTiles,
					'tilesetImage' : tilesetImage,
					'scale' : 1};
	var minimapmapJSON = {	'rows' : rowTileCount,
			'cols' : colTileCount,
			'layers' : array,
			'tilesize' : tilesize,
			'imageNumTiles' : imageNumTiles,
			'tilesetImage' : tilesetImage,
			'scale' : 0.25};
	
    var editor = new MapEditor(mapJSON, canvas, mini);
    
	tilesetImage.onload = function(){editor.map.render(); editor.minimap.render();};
	
	//add a click event to an element to get the click position
	function addClickHandler(element) {
		element.addEventListener('click', function(event) {
			select = (function() {
				return getCoords(event, element);
			})();
			console.log(select);
	    }, false);
	}
	
	
	var select = 0,
		numLayers = editor.map.getLayersNum(), 
		layer = 0,
		leftButtonDown = false;
/*	
	$('#map').on('click', function(event) {
		var x = event.pageX - $(this).offset().left,
			y = event.pageY - $(this).offset().top,
			tile = editor.map.getTile(x,y);
		editor.map.setTile(tile.row, tile.col, layer, select);
		editor.renderTile(tile.row, tile.col);
	});
	
	$('#map').on('click', function(event) {
		var x = event.pageX - $(this).offset().left,
			y = event.pageY - $(this).offset().top,
			tile = editor.map.getTile(x,y);
		editor.map.setTile(tile.row, tile.col, layer, select);
		editor.renderTile(tile.row, tile.col);
		console.log(editor.map);
	});
*/	
	$('#map').on('click', function(event) {
		var x = event.pageX - $(this).offset().left,
			y = event.pageY - $(this).offset().top,
			pos = editor.camera.getPosition(),
			topleft = editor.map.getTile(pos.x, pos.y),
			tile = editor.map.getTile(x + pos.x, y + pos.y);
			
		editor.map.setTile(tile.row, tile.col, layer, select);
		editor.renderTile(tile.row, tile.col);
	});
	
	$('#map').on('contextmenu', function(event) {
		event.preventDefault();
		var x = event.pageX - $(this).offset().left,
			y = event.pageY - $(this).offset().top,
			pos = editor.camera.getPosition(),
			topleft = editor.map.getTile(pos.x, pos.y),
			tile = editor.map.getTile(x + pos.x, y + pos.y);
			
		editor.map.setTile(tile.row, tile.col, layer, 0);
		editor.renderTile(tile.row, tile.col);
	});
	

	$("#file").change(function(e) {	    
	    var image, file;
		var _URL = window.URL || window.webkitURL;
	    if ((file = this.files[0])) {
	        image = new Image();
	        image.onload = function() {
	        	
	        };
	        image.src = _URL.createObjectURL(file);
	    }
	});
	
	$(document).on('submit', '#upload_file', function(e) {
		e.preventDefault();
		$('#upload').html('Uploading...');
		var file = $("#file").val();
		console.log(file);
		request = $.ajax({
			type: "POST",
			url: 'helper.php',
			data: {'upload': file},
			success: function(response){
				if (response != '') {
					$('#upload_error').addClass('error').html(response);
	        	} else {
	        		$('#tileset').attr("src",$(this).val());
	        	}
			}
		});
	});
	
	/*
	$('#tileset').on('mousemove', function(event) {
		event.stopPropagation();
		var x = event.pageX - $(this).offset().left;
		var y = event.pageY - $(this).offset().top;
		var xToTile = x / 32 | 0;
		var yToTile = y / 32 | 0;
		$('#selector').show();
		$('#selector').css({
				left: xToTile * 32 + $(this).offset().left,
				top: yToTile * 32 + $(this).offset().top
		    });
	});
	*/
	$('#tileset').on('click', function(event) {
		event.stopPropagation();
		var x = event.pageX - $(this).offset().left,
			y = event.pageY - $(this).offset().top,
			xToTile = x / 32 | 0,
			yToTile = y / 32 | 0;
		$('#selector2').show();
		$('#selector2').css({
				left: xToTile * 32 + $(this).offset().left,
				top: yToTile * 32 + $(this).offset().top
		    });
		tile = xToTile + yToTile * 16;
		select = tile;
	});
	
	$('#map').off('mouseout', function(event) {
		event.stopPropagation();
		$('#selector').hide();
	});
	
	$('#minimap').on('click', function(event) {
		var x = event.pageX - $(this).offset().left,
			y = event.pageY - $(this).offset().top,
			ctx = $('#map')[0].getContext('2d'),
			pos = editor.camera.getPosition(),
			scale = editor.minimap.getScale(),
			normalized = editor.camera.normalizeCoords(x, y, scale),
			realCoords = editor.camera.checkCoords(normalized.x, normalized.y);
		console.log(x, y, editor.minimap.getScale());
		
		console.log('pos' + pos.x + ' ' + pos.y);
		ctx.translate(pos.x - realCoords.x, pos.y - realCoords.y);
		editor.camera.setPosition(realCoords.x, realCoords.y, 1);
		console.log('bounds' + editor.camera.isGridInBounds(x, y, scale));
    	editor.map.render();
	});
	
	$(document).on('keydown', function(e){
		if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
	    	var ctx = $('#map')[0].getContext('2d'),
		    	tilesize = editor.map.getTilesize(),
				pos = editor.camera.getPosition(),
				transX,transY,newX,newY;

			console.log(e.keyCode);
		    if (e.keyCode == 40) {
		    	// down arrow
		    	transX = 0;
		    	transY = -tilesize;
		    	newX = pos.x;
		    	newY = pos.y + tilesize;
		    } else if (e.keyCode == 39) {
		    	// right arrow
		    	transX = -tilesize;
		    	transY = 0;
		    	newX = pos.x + tilesize;
		    	newY = pos.y;
		    } else if (e.keyCode == 37) {
		    	// left arrow
		    	transX = tilesize;
		    	transY = 0;
		    	newX = pos.x - tilesize;
		    	newY = pos.y;
		    } else if (e.keyCode == 38) {
		    	// up arrow
		    	transX = 0;
		    	transY = tilesize;
		    	newX = pos.x;
		    	newY = pos.y - tilesize;
		    }

			realCoords = editor.camera.checkCoords(newX, newY);
			if (newX != realCoords.x) {
				transX = 0;
			}

			if (newY != realCoords.y) {
				transY = 0;
			}

	    	ctx.translate(transX, transY);
	    	editor.camera.setPosition(realCoords.x, realCoords.y, 1);
	    	editor.map.render();
	    	return false;
	    }
	});
	
	$('#add').on('click', function() {
		$(this).parent().append('<div id="'+numLayers+'"class="layer">layer'+numLayers+'</div>');
		numLayers += 1;
		editor.addLayer();
	});
	
	$('#layers').on('click', '.layer', function(event) {
		$('#'+layer).removeClass('selected');
		event.preventDefault();
		layer = $(this).attr('id');
		$(this).addClass('selected');
		console.log('layer ' + layer);
	});
	
	$('#save_button').on('click', function() {
		var mjson = JSON.stringify(editor.map);
		request = $.ajax({
			type: "POST",
			url: document.URL + "/save",
			data: {'save_map': mjson},
			success: function(response){
				$('#msg').html(response.msg);
			}
			});
	});
	
	for (var index=0; index < numLayers; index++) {
		$('#layers').append('<div id="'+index+'"class="layer">layer'+index+'</div>');
	}
	$('#0').addClass('selected');
	
	//Return the coordinates relative to the element and the tile number
	//To do: move the tile number to the tile class
	function getCoords(event, element){
		var bounding = element.getBoundingClientRect();
		var left = bounding.left;
		var top = bounding.top;
		x = event.pageX - left - document.body.scrollLeft;
		y = event.pageY - top - document.body.scrollTop;
		console.log(element.id + ' - x: ' + x + ', y: ' + y);
		console.log(t = new Tile(x, y));
		console.log(t.getTileNumber);
		return t.getTileNumber;
	}

	//The tile class
	//To do: implement the map as an array of tile objects
	var Tile = function Tile(x, y){
		this.x = x;
		this.y = y;
		this.getTileNumber = Math.floor(this.x / 32) + Math.floor(this.y / 32) * 16;
		this.row = floor(this.x / 32);
		this.col = floor(this.y / 32);
	};
	
	/*
	function addMouseDownHandler(element) {
		element.addEventListener('mousedown', function(event) {
			event.preventDefault();
			var bounding = this.getBoundingClientRect();
			var left = bounding.left;
			var top = bounding.top;
			var x = event.pageX - left - document.body.scrollLeft;
			var y = event.pageY - top - document.body.scrollTop;
			var row = floor(y / 32);
			var col = floor(x / 32);
			var tile = row + col * 16;
			start = tile;
			addDragHandler(element);
		}, false);
	};

	function addDragHandler(element) {
		element.addEventListener('mousemove', drag=function(event) {
			var bounding = this.getBoundingClientRect();
			var left = bounding.left;
			var top = bounding.top;
			var x = event.pageX - left - document.body.scrollLeft;
			var y = event.pageY - top - document.body.scrollTop;
			var row = floor(y / 32);
			var col = floor(x / 32);
			var tile = row + col * 16;
		}, false);
	}

	function addMouseUpHandler(element) {
		element.addEventListener('mouseup', function(event) {
			element.removeEventListener('mousemove', drag);
			var bounding = this.getBoundingClientRect();
			var left = bounding.left;
			var top = bounding.top;
			var x = event.pageX - left - document.body.scrollLeft;
			var y = event.pageY - top - document.body.scrollTop;
			var row = floor(y / 32);
			var col = floor(x / 32);
			var tile = row + col * 16;
			end = tile;
			console.log('s,e: ' + start, end);
		}, false);
	}
	*/
	};
	initMapEditorApp();
});
