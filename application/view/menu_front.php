<div class="login_form" id="login_form">
		<div class="close" onclick="$('#login_form').hide(); $('#outer').css('background-color', 'transparent');$('#outer').css('z-index', '0');"></div>
	<div id="login">
	<form method="post" id="log_me_in" action="<?php echo URL;?>login/login">
		<label for="Username">Username &nbsp;</label><input type="text" id="login_username" name="username"/><span id="login_error"></span>
		<label for="Password">Password &nbsp;</label><input type="password" id="login_pass" name="password"/>
		<label for="Remember">Remember me &nbsp;</label><input type="checkbox" id="remember" name="remember"/>
		<input id="submit" type="submit" name="login" value="login"/>
	</form>
	<p>Don't have an account yet? Register <span onclick="$('#login').hide(); $('#register').show();">here.</span></p>
	<br/>
	</div>
	<div id="register">
	<form method="post" id="reg_form" action="<?php echo URL;?>login/register">
		<label for="Username">Username &nbsp;</label><input type="text" id="reg_username" name="username"/><span id="user_error"></span>
		<label for="Email">Email &nbsp;</label><input type="text" id="email" name="email"/><span id="email_error"></span>
		<label for="Password">Password &nbsp;</label><input type="password" id="reg_pass" name="password"/><span id="pass_error"></span>
		<label for="Rpassword">Repeat &nbsp;</label><input type="password" id="reg_rpass" name="rpassword"/><span id="rpass_error"></span>
		<input id="submit" type="submit" name="register" value="register"/>
	</form>
	<p>Already have an account? Login <span onclick="$('#login').show(); $('#register').hide();">here.</span></p>
	</div>
</div>
<div  style="width: 100%;"id="outer"></div>
<div class="outer-wrap">
<div class="inner-wrap">
	<div class="header">
		<div class="logo"></div>
		<header class="clear">
				<nav id="nav_menu">
					<ul>
						<li><a href="/" class="home" title="Home">Home</a></li>
						<li><a href="<?php echo URL;?>mapeditor" class="media" title="Media">Media</a></li>
						<li><a href="#" class="game_info" title="Game info">Game info</a></li>
						<li><a href="<?php echo URL;?>help" class="faq"  title="FAQ">FAQ</a></li>
					</ul>
				</nav>
		</header>
	</div>