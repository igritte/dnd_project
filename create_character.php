<?php
require_once 'helper.php';
require_once 'header.php';
get_header();
define('FREE_STATS', 20);
?>
	<body>
	<div class="wrap">
		<div class="header">
			<div class="logo"></div>
			<header class="clear">
					<nav id="nav_menu">
						<ul>
							<li><a href="#" class="home" title="Home">Home</a></li>
							<li><a href="map.php" class="media" title="Media">Media</a></li>
							<li><a href="#" class="tavern" title="Tavern">Tavern</a></li>
							<li><a href="#" class="game_info" title="Game info">Game info</a></li>
							<li><a href="#" class="faq"  title="FAQ">FAQ</a></li>
						</ul>
					</nav>
					<div id="profile">
						<div class="char_info">
						<?php if (!is_logged_in()) {
							echo "You're not logged in. Login or Register to start playing.";
						} else if (!selected_char()) {
							echo "You haven't selected a character yet. Create one here.";
						} else {
							echo "You're playing as " . $_SESSION['char'];
						}
						?></div>
						<div id="avatar"><img src="src/tokens/male_dwarf.png"></div>
						<div id="profile_options">
							<ul id="options">
								<li>Manage profile</li>
								<li>Create a new Character</li>
								<li>Create a party</li>
							</ul>
						</div>
					</div>
			</header>
		</div>
		<div id="main">
		<div class="page">
				<div class="content">
					<form id="character_creation">
					<label>Name: </label><input name="name"/>HP<span id="hp"></span>Initiative<span id="initiative"></span></span>Defenses<span id="defense"></span>
					<div id="gender">
						<p class="picker">Choose a gender</p>
						<label>Male </label><input type="radio" name="gender" value="male"/>
						<label>Female </label><input type="radio" name="gender" value="female"/>
					</div>
					<div id="race">
						<p class="picker">Choose a race</p>
						<?php get_race_selection(); ?>
					</div>
					<div id="class">
						<p class="picker">Choose a class</p>
						<?php get_class_selection(); ?>
					</div>
					<div id="bonus">
							
					</div>
					<div id="stats">
						<p class="picker">Stats</p>
						<div class="stat_picker">
						Free points: <div id="free_stats">20</div>
						<?php $stats = get_stats(); 
							foreach ($stats as $stat => $value) {
								echo "<div class=\"stats\">$stat<span class=\"minus\">-</span><span id=\"$stat\">$value</span><span class=\"plus\">+</span></div>";
							}
						?>
						</div>
						<div id="skills">
						<p class="picker" id="skill_picker" style="color:white">Choose your skills</p>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
		</div>
	</body>
	<script>console.log($('#main').width());</script>
<script>
var race = '',
	gender = '',
	selectedClass = '',
	freeStats = 20;
$( document ).ready(function() {
    $('#avatar').bind('contextmenu', function(event) {
    	event.preventDefault();
		$("#profile_options").fadeIn(500, startFocusOut());
    });
    $('#profile').bind('contextmenu', function(event) {
    	event.preventDefault();
    });
    $('.picker').bind('click', function(event) {
    	console.log($(this).children());
    });
    $('.minus').bind('click', function(event) {
    	var value = parseInt($(this).next().html());
    	if (value > 8) {
    		decrease(value, $(this).next());
    		$('#free_stats').html(freeStats);
    	}
    });
    $('.plus').bind('click', function(event) {
    	var value = parseInt($(this).prev().html());
    	increase(value, $(this).prev());
    	$('#free_stats').html(freeStats);
    });
    $('.race_img').bind('click', function(event) {
    	if (race != '') {
     		$('#'+race).removeClass('chosen');
     	}
     	if (race == 'human') {
     		$('#bonus').show();
     	} else {
			$('#bonus').hide();
     	}
    	race = this.id;
    	$(this).addClass(' chosen');
    	console.log(race);
    });
     $('.class_picker').bind('click', function(event) {
     	if (selectedClass != '') {
     		$('#'+selectedClass).removeClass('chosen');
     	}
     	console.log(this);
     	$(this).addClass(' chosen');
    	selectedClass = this.id;
    	console.log(selectedClass);
    });
    $('#gender input').click(function() {
    	gender = $(this).val();
    	if (gender == 'female')
    		$('.race_img').addClass(gender);
    	else
    		$('.race_img').removeClass('female');
    });
    $('#skill_picker').on('click', function() {
		if (selectedClass != '') {
			request = $.ajax({
				type: "POST",
				url: 'helper.php',
				data: {'skills': selectedClass},
				success: function(response){
					response = JSON.parse(response);
					response = response.selectedClass;
					console.log(response);
					if (response != '') {
						$.each(response, function(i, val) {
							console.log(i, val);
							//$('#skills').append('<div id="' + this + '"></div>');
						});
					}
		        }
			});
		}
    });
});
function startFocusOut() {
    $(document).on("click", function () {   
        $("#profile_options").hide(500);              // To hide the context menu
        $(document).off("click");           
    });
}
function increase(value, element) {
	value += 1;
	if (value >= 10 && value < 14) {
		if (freeStats >= 1) {
			freeStats -= 1;
			element.html(value);
		}
	} else if (value >= 14 && value < 17) {
		if (freeStats >= 2) {
			freeStats -= 2;
			element.html(value);
		}
	} else if (value == 17) {
		if (freeStats >= 3) {
			freeStats -= 3;
			element.html(value);
		}
	} else if (value >= 18) {
		if (freeStats >= 4) {
			freeStats -= 4;
			element.html(value);
		}
	}
}
function decrease(value, element) {
	value -= 1;
	if (value >= 8 && value < 14) {
		freeStats += 1;
		element.html(value);
	} else if (value >= 8 && value < 14) {
		freeStats += 1;
		element.html(value);
	} else if (value >= 14 && value < 17) {
		freeStats += 2;
		element.html(value);
	} else if (value == 17) {
		freeStats += 3;
		element.html(value);
	} else if (value >= 18) {
		freeStats += 4;
		element.html(value);
	}
}
</script>
</html>