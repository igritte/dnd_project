<?php
class Mapeditor extends BaseController
{
	function __construct()
	{
		Auth::isUserOnline();
		parent::__construct();

	}

	public function index()
	{
		if (isset($_SESSION['editing'])) {
			$this->view->content = $this->model->edit($_SESSION['editing']);
			$this->view->render('map_editor/index');
		} else {
			$this->view->render('map_editor/index');
		}
	}
	
	public function save()
	{
		if (isset($_POST['save_map'])) {
			$this->model->save($_POST['save_map']);
			echo json_encode( array( "status" => "success", "msg" => "Great! Now test you new map."));
		} else {
			echo json_encode( array( "status" => "fail", "msg" => "There was an error while saving the map."));
		}
	}
	
	public function editMap()
	{
		if (isset($_POST['edit'])) {
			$_SESSION['editing'] = $_POST['edit'];
			$this->view->content = $this->model->edit($_POST['edit']);
			$this->view->render('map_editor/index');
		} else {
			echo json_encode( array( "status" => "fail", "msg" => "There was an error."));
		}
	}
	
	public function clear()
	{
		if (isset($_SESSION['editing'])) {
			unset($_SESSION['editing']);
			$this->view->render('map_editor/index');
		}
	}
}